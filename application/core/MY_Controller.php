<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
    
    public $contenido = false;

    public function __construct() {
        parent::__construct();

        $this->contenido = (object)array(
            'error' => false,
            'codigo' => 200,
            'data' => array(),
            'mensaje' => false
        );

    }

    public function response($contenido =false){
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode(($contenido != false? $contenido : $this->contenido), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;  
    }
}