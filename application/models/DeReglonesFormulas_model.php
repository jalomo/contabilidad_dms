<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeReglonesFormulas_model extends CI_Model {

    public function get_list($where = false)
    {
        $this->db
            ->select([
                'de_reglones_formulas.*', 
                'cuentas_dms.cuenta as clave_cuenta', 
                'cuentas_dms.decripcion as nombre_cuenta',
                'de_reporte_reglones.nombre as reporte_reglones',
                'ca_reportes.nombre as reporte'
            ])
            ->from('de_reglones_formulas')
            ->join('cuentas_dms', 'de_reglones_formulas.cuenta_id = cuentas_dms.id','left')
            ->join('de_reporte_reglones', 'de_reglones_formulas.reporte_reglon_id_ref = de_reporte_reglones.id','left')
            ->join('ca_reportes', 'de_reporte_reglones.reporte_id = ca_reportes.id','left')
            ->where('de_reglones_formulas.deleted_at IS NULL',null, false)
            ->order_by('de_reglones_formulas.orden','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_reglones_formulas.deleted_at IS NULL', null, false);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        $uuid = utils::guid();
        $this->db->set('id',$uuid);
        $this->db->set('created_at',utils::now());
        $response = $this->db->insert('de_reglones_formulas', $contents);
        return $response;
    }

    public function update($contents,$where)
    {
        $this->db->where($where);
        $this->db->set('updated_at',utils::now());
        return $this->db->update('de_reglones_formulas', $contents);
    }

    public function delete($where = false){
        $response = 0;
        if(is_array($where) && count($where)>0){
            $this->db->where($where);
            $response = $this->db->delete('de_reglones_formulas');
        }
        return $response;
    }

    public function get($where = false){
        $this->db
            ->from('de_reglones_formulas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('de_reglones_formulas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function get_max_orden($where = false){
        $this->db->select('orden');
        $this->db
            ->from('de_reglones_formulas');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->order_by('orden','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

}