<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CaOrigenTransaccion_model extends CI_Model {

    public function get($where = false)
    {
        $this->db->from('ca_origen_transaccion');
        if(is_array($where)){
            $this->db->where($where);
        }
        // $this->db->where('deleted_at IS NULL', null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function get_list($where = false)
    {
        $this->db->from('ca_origen_transaccion');
        if(is_array($where)){
            $this->db->where($where);
        }
        // $this->db->where('deleted_at IS NULL', null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function insert($contents)
    {
        $this->db->set('created_at',utils::now());
        $this->db->set('updated_at',utils::now());
        $this->db->insert('polizas', $contents);
        $this->db->insert_id();
    }


}