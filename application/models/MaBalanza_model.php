<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MaBalanza_model extends CI_Model {

    public function get_list($where = false)
    {
        $this->db->select([
                'id', 
                'cuenta_id', 
                'cuenta', 
                'mes', 
                'anio', 
                'saldo_inicial', 
                'cargos', 
                'abonos', 
                'saldo_mes', 
                'saldo_actual', 
                'ultimo_asiento_aplicado'
            ])
            ->from('ma_balanza')
            ->where('ma_balanza.deleted_at IS NULL',null, false);
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

}