<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DeIngresosDiariosActivos_model extends CI_Model {

    // public function insert($contents)
    // {
    //     $this->db->set('created_at',utils::now());
    //     $response = $this->db->insert('ca_reportes', $contents);
    //     return $response;
    // }

    // public function update($contents,$where)
    // {
    //     $this->db->where($where);
    //     $this->db->set('updated_at',utils::now());
    //     return $this->db->update('ca_reportes', $contents);
    // }

    // public function delete($where = false){
    //     $response = 0;
    //     if(is_array($where) && count($where)>0){
    //         $this->db->where($where);
    //         $response = $this->db->delete('ca_reportes');
    //     }
    //     return $response;
    // }

    public function get($where = false){
        $this->db
            ->from('de_ingresos_diarios_activos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

    public function getAll($where = false){
        $this->db
            ->from('de_ingresos_diarios_activos')
            ->order_by('de_ingresos_diarios_activos.id','asc');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_ingresos_diarios_activos.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }


    public function getSum($where = false){
        $this->db
            ->select_sum('monto')
            ->from('de_ingresos');
        if(is_array($where)){
            $this->db->where($where);
        }
        $this->db->where('de_ingresos.deleted_at IS NULL',null, false);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row_array() : false;
    }

}