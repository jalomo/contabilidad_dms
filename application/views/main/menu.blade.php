<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"></div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url(); ?>/administrador/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Inicio</span></a>
    </li>
    <?php
    if ($this->session->userdata('perfil_id') == 1) { ?>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdministrador" aria-expanded="true" aria-controls="collapseAdministrador">
            <i class="fas fa-fw fa-folder"></i>
            <span>Administrar</span>
        </a>
        <div id="collapseAdministrador" class="collapse" aria-labelledby="" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('administrador/usuarios') ?>">Usuarios</a>
                <a class="collapse-item" href="<?php echo site_url('administrador/reportes') ?>">Reportes</a>
                <a class="collapse-item" href="<?php echo site_url('administrador/cuentas') ?>">Cuentas</a>
            </div>
        </div>
    </li>
    <?php } ?>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/polizas/lista_polizas">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Ver Polizas</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/cuentas/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Cuentas</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/balanza_comprobacion/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Balanza de comprobación</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/autorizaciones/clientes">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Autorizar Creditos</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/cobranza/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Cobranza</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/facturacion/lista">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Facturación</span></a>
    </li>
    <!-- <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReportes" aria-expanded="true" aria-controls="collapseReportes">
            <i class="fas fa-fw fa-folder"></i>
            <span>Balanzas</span>
        </a>
        <div id="collapseReportes" class="collapse" aria-labelledby="headingReportes" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('reportes/balanza_mylsa') ?>">Balanza Mylsa</a>
                <a class="collapse-item" href="<?php echo site_url('reportes/balance_consejo') ?>">Balance Consejo</a>
                <a class="collapse-item" href="<?php echo site_url('reportes/balance_ford') ?>">Balance Ford</a>
            </div>
        </div>
    </li> -->
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEstadosFinancieros" aria-expanded="true" aria-controls="collapseEstadosFinancieros">
            <i class="fas fa-fw fa-folder"></i>
            <span>Reportes</span>
        </a>
        <?php $reportes = $this->session->userdata('reportes'); ?>
        <div id="collapseEstadosFinancieros" class="collapse" aria-labelledby="headingReportes" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @foreach ($reportes as $reporte)
                <a class="collapse-item" href="<?php echo site_url("estados_financieros/generico") . '?reporte_id='.base64_encode($reporte['id']) ?>">{{ $reporte['nombre'] }}</a>
                @endforeach
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFormatosEfectivo" aria-expanded="true" aria-controls="collapseFormatosEfectivo">
            <i class="fas fa-fw fa-folder"></i>
            <span>Formatos de efectivo</span>
        </a>
        <div id="collapseFormatosEfectivo" class="collapse" aria-labelledby="headingReportes" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/flujo_efectivo') ?>">Formato Flujo de efectivo</a>
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/flujo_bancos_crud/index') ?>">Formato de flujo de bancos</a>
                <!-- <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/flujo_bancos') ?>">Formato de flujo de bancos</a> -->
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/ingresos') ?>">Ingresos</a>
                <a class="collapse-item" href="<?php echo site_url('formatos_efectivo/ingresos_diarios') ?>">Ingresos diarios</a>
            </div>
        </div>
    </li>
    <!-- <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/servicio_gral/ServicioGral">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Servicio Gral</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/estado_resultados/ResultadosSanjuan">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Edo. Res. Analitico</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/refacciones_sjr/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Refacciones SJR</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>index.php/seminuevos/reporte/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Seminuevos</span></a>
    </li> -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>