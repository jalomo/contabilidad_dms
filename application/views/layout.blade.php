<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<title>Contabilidad DMS SAN JUAN</title>

<head>
    <meta charset="ISO-8859-1">
    @include('main/head')
    @yield('included_css')
    @yield('style')
   
    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var PATH_BASE = "<?php echo base_url(); ?>";
        var API_URL_DMS = "<?php echo API_URL_DMS ?>";
        var PATH_LANGUAGE = "<?php echo 'https://cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json'; ?>";
    </script>
</head>

<body id="page-top">
    <div id="wrapper">
        @include('main/menu')

        <div id="content-wrapper" class="d-flex flex-column">
            <div class="headertitle">
                <div class="row">
                    <div class="col-md-11">
                        <h4 class="ml-3 mt-2">Contabilidad DMS <small>- SAN JUAN</small></h4>
                    </div>
                    <div class="col-md-1">
                        <a href="<?php echo site_url('administrador/logout')?>"><i style="color:#fff" class="fas fa-fw fa-sign-out-alt fa-2x mt-2"></i></a><br/>
                        <small>Cerrar sesión</small>
                    </div>
                </div>
            </div>
            <div id="content" class="">
                <div class="p-5">
                    @yield('contenido')
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; www.sohex.mx</span>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!--<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>-->
</body>
@yield('script')

</html>