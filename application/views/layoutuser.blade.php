<!DOCTYPE html>
<html lang="en">
<title>Contabilidad DMS SAN JUAN</title>
<head>
    @include('main/head')
    @yield('included_css')
    @yield('style')
    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var PATH_BASE = "<?php echo base_url(); ?>";
        var API_URL_DMS = "<?php echo API_URL_DMS ?>";
        var PATH_LANGUAGE = "<?php echo 'https://cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json'; ?>";
    </script>
</head>

<body id="page-top">
    <div id="wrapper">
        @include('main/menu')
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <div class="container-fluid">
                    @yield('contenido')
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; www.sohex.mx</span>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
</body>
@yield('script')

</html>