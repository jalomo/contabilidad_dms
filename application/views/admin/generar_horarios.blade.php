@layout('layout')
@section('included_css')
    <link href="statics/css/bootstrap-datetimepicker.css" rel="stylesheet">
@endsection
@section('contenido')
    <form id="frm">
        <input type="hidden" name="fecha_apartir" id="fecha_apartir" value="{{ $fecha_apartir }}">
        <div class="row">
            <div class="col-sm-3">
                <label class="control-label mb-1">Estado</label>
                {{ $id_estado }}
                <span class="error error_id_estado"></span>
            </div>
            <div class="col-sm-3">
                <label class="control-label mb-1">Sucursal</label>
                {{ $id_sucursal_ubicacion }}
                <span class="error error_id_sucursal_ubicacion"></span>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <label for="">A partir de</label>
                <input required="" type="text" disabled="" class="form-control" name="cita" id="cita"
                    value="{{ date_eng2esp_1($fecha_apartir) }}">
            </div>
            <div class='col-sm-3'>
                <label for="">Selecciona la fecha fin</label>
                <div class="form-group1">
                    <div class='input-group date' id='datetimepicker1'>
                        <input id="fecha_hasta" name="fecha_hasta" type='text' class="form-control" value="" required="" />
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
                <span class="error_fecha"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <br>
                <button id="generar" class="btn btn-success">Generar</button>
            </div>
        </div>
    </form>
@endsection
@section('included_js')
    <script src="statics/js/moment.js"></script>
    <script src="statics/js/bootstrap-datetimepicker.js"></script>
    <script src="statics/js/servicio.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var id_cita = '';
        var accion = '';
        $("body").on("click", '#generar', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            if ($("#fecha_hasta").val() == '' || $("#fecha_apartir").val() == '') {
                ErrorCustom("Es necesario ingresar los dos campos");
            } else {
                ConfirmCustom("¿Está seguro de generar horarios?", callbackGenerar, "", "Confirmar", "Cancelar");
            }
        });
        function callbackGenerar() {
            var url = site_url + "/admin/generarhorariosAux/";
            ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                if (result == -1) {
                    ErrorCustom('La fecha fin debe ser mayor');
                }else if(result == -2) {
                    ErrorCustom('No existen lavadores asignados a esa sucursal');
                }else {
                    ExitoCustom("Horarios generados correctamente", function() {
                        window.location.reload();
                    });

                }
            });
        }
        const getFecha = () => {
            var url = site_url + "/admin/getFechaPartir";
            ajaxJson(url, {
                id_sucursal: $("#id_sucursal_ubicacion").val()
            }, "POST", "", function(result) {
                result = JSON.parse(result);
                $("#cita").val(result.fecha)
                $("#fecha_apartir").val(result.fecha)
                $('#datetimepicker1').datetimepicker({
                    minDate: result.fecha,
                    format: 'DD/MM/YYYY',
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    locale: 'es'
                });
            });
        }
        $("#id_sucursal_ubicacion").on("change", getFecha);

    </script>
@endsection
