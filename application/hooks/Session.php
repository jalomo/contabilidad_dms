<?php

class Session
{

    public $CI;

    function __construct()
    {
    }

    public function check_auth($params)
    {

        $this->CI = &get_instance();
        if ($this->CI->router->class != 'api') {
            $modulos_libres = $params['modulos_libres'];

            if (!$this->CI->input->is_ajax_request() && !in_array($this->CI->router->module, $modulos_libres)) {
                if (!$this->CI->session->userdata('logged_in')) {
                    redirect('inicio/index');
                }
            }  
            if ($this->CI->session->userdata('perfil_id') != 1 && $this->CI->router->module == 'administrador' && $this->CI->router->class == 'reportes') {
                redirect('inicio/index');
            }
        }
    }
}
