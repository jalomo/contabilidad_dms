@layout('layout')

@section('contenido')
    <style>
        .title_table {
            font-weight: bold;
            background: #eee !important;
            color: #323232;
        }

        .caption_table {
            font-weight: bold;
            background: #4E73DF;
            color: #fff;
        }

        .table td,
        .table th {
            padding: .45rem !important;
            vertical-align: top;
            border-top: 1px solid #e3e6f0;
            font-size: 14px !important;
        }

        .bold {
            font-weight: bold;
        }

    </style>

    <h4 class="mt-4 mb-4 text-gray-800 text-center">
        SERVICIO <br />
        RESULTADOS DE OPERACIÓN
    </h4>

    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                
                <th class="title_table"></th>
                <th class="caption_table">ENE.20</th>
                <th class="caption_table">FEB.20</th>
                <th class="caption_table">MZO.20</th>
                <th class="caption_table">ABR.20</th>
                <th class="caption_table">MAY.20</th>
                <th class="caption_table">JUN.20</th>
                <th class="caption_table">JUL.20</th>
                <th class="caption_table">AGO.20</th>
                <th class="caption_table">SEPT.20</th>
                <th class="caption_table">OCT.20</th>
                <th class="caption_table">NOV.20</th>
                <th class="caption_table">DIC.20</th>
                <th class="caption_table">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="caption_table">SERVICIO TRADICIONAL</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class=""> $4,365.60 </td>
            </tr>
            <tr>
                <th class="caption_table">POLÍTICA Y GARANTÍAS</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">CARGOS INTERNOS</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">TOTAL DE ORDENES</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <td class="title_table" colspan="14"><b> </b></td>
            </tr>
            <tr>
                <th class="caption_table">VENTAS DE TALLER</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">VTA. PROM. POR ORDEN </th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>

            <tr>
                <th class="caption_table">MANO DE OBRA</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">INCENTIVOS FORD</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>

            <tr>
                <td class="title_table" colspan="14"><b> </b></td>
            </tr>
            <tr>
                <th class="caption_table">VENTAS GARANTIAS</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class=""> $4,365.60 </td>
            </tr>
            <tr>
                <th class="caption_table">VTA. PROM. POR ORDEN </th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>

            </tr>
            <tr>
                <th class="caption_table">MANO DE OBRA</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>

            </tr>
            <tr>
                <th class="caption_table">VTAS T. FUERA DE TALLER</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>

            </tr>
            <tr>
                <th class="caption_table"> TOTAL VENTAS</th>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>

                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
            </tr>
            <tr>
                <th class="title_table" colspan="14">COSTO DE VENTAS:</th>
            </tr>
            <tr>
                <td class="caption_table"> MANO DE OBRA</td>
                <th class="">$0.00</th>

                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> MATERIALES DIVERSOS Y TOT</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> TOTAL COSTO DE VENTAS</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table">UTILIDAD BRUTA</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> % UTILIDAD BRUTA </td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">GASTOS DE VENTA:</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">SUELDOS OTROS</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">PRESTACIONES</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">IMSS, SAR E INFONAVIT</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">PUBLICIDAD</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">HERRAMIENTAS Y FLETES</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">ENTRENAM. Y CAPACITA.</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">CORTESIAS</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">UNIFORMES</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">MANTENIM UNIDADES Y EQPOS</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">GASOLINA</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">GASTOS DIVERSOS </td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">TOTAL GTOS. DE VENTA</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">% SOBRE VENTAS</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">UTILIDAD DEPARTAMENTAL</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">ANTES DE TRANSFERENCIA</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">TRANSF. DE UTILIDAD</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">UTILIDAD AJUSTADA</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
            <tr>
                <td class="caption_table">% SOBRE VENTAS</td>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
                <th>$0.00</th>
            </tr>
        </tbody>
    </table>


@endsection
