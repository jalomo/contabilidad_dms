@layout('layout')


@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    BALANZA COMPROBACIÓN COMPLETA <?php echo utils::mes_espanol($mes); ?> <?php echo $anio; ?>
</h4>


<div class="card mt-5">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo site_url('balanza_comprobacion'); ?>" class="btn btn-sm btn-secondary float-right"
                    type="button"><i class="fas fa-arrow-circle-left"></i> Regresar</a>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body">
        <div class="row ">
            <div class="col-12">

                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Sal Inicial</th>
                            <th>Cargos</th>
                            <th>Abono</th>
                            <th>Saldo Mes</th>
                            <th>Saldo Actual</th>


                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Sal Inicial</th>
                            <th>Cargos</th>
                            <th>Abono</th>
                            <th>Saldo Mes</th>
                            <th>Saldo Actual</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php if(is_array($balanza)){ ?>
                        <?php foreach($balanza as $value_mes){ ?>
                        <tr>
                            <td><?php echo $value_mes['cuenta'];?></td>
                            <td><?php echo $value_mes['descripcion'];?></td>
                            <td><?php echo ($value_mes['cargos'] > 0 || $value_mes['abonos'] > 0 || $value_mes['saldo_mes'] > 0)? '$ '.utils::formatMoney($value_mes['saldo_inicial'])  : '<center>-</center>';?>
                            </td>
                            <td><?php echo ($value_mes['cargos'] > 0)? '$ '.utils::formatMoney($value_mes['cargos']) : '<center>-</center>';?>
                            </td>
                            <td><?php echo ($value_mes['abonos'] > 0)? '$ '.utils::formatMoney($value_mes['abonos']) : '<center>-</center>';?>
                            </td>
                            <td><?php echo ($value_mes['saldo_mes'] > 0)? '$ '.utils::formatMoney($value_mes['saldo_mes']) : '<center>-</center>';?>
                            </td>
                            <td><?php echo ($value_mes['cargos'] > 0 || $value_mes['abonos'] > 0 || $value_mes['saldo_mes'] > 0)? '$ '.utils::formatMoney($value_mes['saldo_actual']) : '<center>-</center>';?>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>


@endsection

@section('style')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/dataTables.bootstrap4.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous"
    referrerpolicy="no-referrer" />

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
</style>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script>
    $.extend(true, $.fn.dataTable.defaults, {
        "language": {
            "decimal": ",",
            "thousands": ".",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoPostFix": "",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "loadingRecords": "Cargando...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "processing": "Procesando...",
            "search": "Buscar:",
            "searchPlaceholder": "Término de búsqueda",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "aria": {
                "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            //only works for built-in buttons, not for custom buttons
            "buttons": {
                "create": "Nuevo",
                "edit": "Cambiar",
                "remove": "Borrar",
                "copy": "Copiar",
                "csv": "fichero CSV",
                "excel": "tabla Excel",
                "pdf": "documento PDF",
                "print": "Imprimir",
                "colvis": "Visibilidad columnas",
                "collection": "Colección",
                "upload": "Seleccione fichero...."
            },
            "select": {
                "rows": {
                    _: '%d filas seleccionadas',
                    0: 'clic fila para seleccionar',
                    1: 'una fila seleccionada'
                }
            }
        }
    });
    $(document).ready(function () {
        $('table#dataTable').dataTable({
            "pageLength": 50
        });
    });
</script>
@endsection