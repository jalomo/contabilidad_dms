<?php

class MaBalanza_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getList_anios(){
        $this->db->distinct();
        $this->db->select('anio');
        $this->db->from('ma_balanza');
        $this->db->order_by('anio','asc');

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getList_mes($anio){
        $this->db->distinct();
        $this->db->select('mes');
        $this->db->from('ma_balanza');
        $this->db->where('anio',$anio);
        $this->db->order_by('mes','asc');

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function getList_balanza($mes,$anio){
        $database = $this->load->database('default', TRUE);
        $query = $database->query("call pa_obtener_balanza('".$mes."','".$anio."');");
        $database->close();
        return $query->result_array();
    }

    public function getList_balanza_simple($mes,$anio){
        $database = $this->load->database('default', TRUE);
        $query = $database->query("call pa_obtener_balanza_simple('".$mes."','".$anio."');");
        $database->close();
        return $query->result_array();
    }
}