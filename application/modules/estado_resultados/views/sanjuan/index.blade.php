@layout('layout')

@section('contenido')
<style>
    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
    }
</style>


<h4 class="mt-4 mb-4 text-gray-800 text-center">
    ESTADO DE RESULTADOS ANALÍTICO COMPARATIVO<br />
    SAN JUAN DEL RIO
</h4>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="title_table"></th>
            <th class="caption_table">ENE.<?php echo date('Y'); ?></th>
            <th class="caption_table">FEB.<?php echo date('Y'); ?></th>
            <th class="caption_table">MZO. <?php echo date('Y'); ?></th>
            <th class="caption_table">ABR.<?php echo date('Y'); ?></th>
            <th class="caption_table">MAY.<?php echo date('Y'); ?></th>
            <th class="caption_table">JUN.<?php echo date('Y'); ?></th>
            <th class="caption_table">JUL.<?php echo date('Y'); ?></th>
            <th class="caption_table">AGO.<?php echo date('Y'); ?></th>
            <th class="caption_table">SEPT.<?php echo date('Y'); ?></th>
            <th class="caption_table">OCT.<?php echo date('Y'); ?></th>
            <th class="caption_table">NOV.<?php echo date('Y'); ?></th>
            <th class="caption_table">DIC.<?php echo date('Y'); ?></th>
            <th class="caption_table">TOTAL</th>
        </tr>
    </thead>
    <tbody>

        <?php foreach ($cuentas as $key => $cuenta) { 
            if ($cuenta['title']) {?>
            <tr>
                <td style="text-transform: uppercase;" class="title_table" colspan="14"><b><?php echo $cuenta['title'];?></b></td>
            </tr>
            <?php } else { ?> 
            <tr>
                <td class="caption_table" style="text-transform: uppercase;"> <?php echo $cuenta['nombre']; ?></td>
                <?php for ($i = 1; $i <= 12; $i++) { ?>
                    <td class="text-dark"> <?php echo $cuenta['meses'][$i] ? utils::toMoney($cuenta['meses'][$i]) : utils::toMoney(0); ?></td>
                <?php } ?>
                <td  class="text-dark"> <?php echo $cuenta['total'] ? utils::toMoney($cuenta['total']) : utils::toMoney(0); ?></td>
            </tr>
        <?php }
        } ?>
    </tbody>
</table>

<script>
    $(function(){
        setTimeout(() => {

            for (let i = 1; i < 14; i++) {        
                var suma = 0; 
                $('table#dataTable tr').each(function( index ) { 
                    var dom_td = $(this).children('td');
                    if ($.trim(dom_td.eq(0).text()).toUpperCase() == 'UTILIDAD DE OPERACIÓN' ){
                        dom_td.eq(i).html( '$ '+parseFloat(suma).toFixed(2) );
                        return false;       
                    }else{   
                        var contenido = $.trim(dom_td.eq(i).text().split('$').join('').split(',').join('') ); 
                        var twoPlacedFloat = parseFloat(contenido);
                        if(!isNaN(twoPlacedFloat)){
                            suma = parseFloat(suma) + parseFloat(twoPlacedFloat); 
                        }   
                    }  
                });

            }


            for (let i = 1; i < 14; i++) {
            
                var suma = 0; 
                $('table#dataTable tr').each(function( index ) { 
                    var dom_td = $(this).children('td');
                    if ($.trim(dom_td.eq(0).text()) == 'TOTAL VENTAS' ){

                        dom_td.eq(i).html( '$ '+parseFloat(suma).toFixed(2) );
                        return false;       
                    }else{   
                        var contenido = $.trim(dom_td.eq(i).text().split('$').join('').split(',').join('') ); 
                        var twoPlacedFloat = parseFloat(contenido);
                        if(!isNaN(twoPlacedFloat)){
                            suma = parseFloat(suma) + parseFloat(twoPlacedFloat); 
                        }   
                    }  
                });

            }


            for (let i = 1; i < 14; i++) {
            
                var suma = 0; 
                var inicio = false;
                $('table#dataTable tr').each(function( index ) { 
                    var dom_td = $(this).children('td');
                    if(inicio == false){
                        inicio = ($.trim(dom_td.eq(0).text()) == 'UTILIDAD BRUTA')? true : false;
                    }else{   
                        if ($.trim(dom_td.eq(0).text()) == 'TOTAL' ){

                            dom_td.eq(i).html( '$ '+parseFloat(suma).toFixed(2) );
                            return false;       
                        }else{   
                            var contenido = $.trim(dom_td.eq(i).text().split('$').join('').split(',').join('') ); 
                            var twoPlacedFloat = parseFloat(contenido);
                            if(!isNaN(twoPlacedFloat)){
                                suma = parseFloat(suma) + parseFloat(twoPlacedFloat); 
                            }   
                        }  
                    }
                });

            }


            for (let i = 1; i < 14; i++) {
                var suma = 0; 
                var inicio = false;
                $('table#dataTable tr').each(function( index ) { 
                    var dom_td = $(this).children('td');
                    if(inicio == false){
                        inicio = ($.trim(dom_td.eq(0).text()) == 'SUELDOS Y COMISIONES')? true : false;
                    }else{   
                        if ($.trim(dom_td.eq(0).text()) == 'TOTAL GASTOS' || $.trim(dom_td.eq(0).text()) == 'Total gastos' ){
                            console.log($.trim(dom_td.eq(0).text()));
                            dom_td.eq(i).html( '$ '+parseFloat(suma).toFixed(2) );
                            return false;       
                        }else{   
                            var contenido = $.trim(dom_td.eq(i).text().split('$').join('').split(',').join('') ); 
                            var twoPlacedFloat = parseFloat(contenido);
                            if(!isNaN(twoPlacedFloat)){
                                suma = parseFloat(suma) + parseFloat(twoPlacedFloat); 
                            }   
                        }  
                    }
                });
            }

            
        }, 400);
        
    })
</script>


@endsection