@layout('layout')

@section('contenido')
    <style>
        .title_table {
            font-weight: bold;
            background: #eee !important;
            color: #323232;
        }

        .caption_table {
            font-weight: bold;
            background: #4E73DF;
            color: #fff;
        }

        .table td,
        .table th {
            padding: .45rem !important;
            vertical-align: top;
            border-top: 1px solid #e3e6f0;
            font-size: 14px !important;
        }

        .bold {
            font-weight: bold;
        }

    </style>

    <h4 class="mt-4 mb-4 text-gray-800 text-center">
        ESTADO DE RESULTADOS ANALÍTICO COMPARATIVO<br />
        SAN JUAN DEL RIO
    </h4>

    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="title_table"></th>
                <th class="caption_table">ENE.20</th>
                <th class="caption_table">FEB.20</th>
                <th class="caption_table">MZO. 20</th>
                <th class="caption_table">ABR.20</th>
                <th class="caption_table">MAY.20</th>
                <th class="caption_table">JUN.20</th>
                <th class="caption_table">JUL.20</th>
                <th class="caption_table">AGO.20</th>
                <th class="caption_table">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="title_table" colspan="14"><b> Volúmen Ventas </b></td>
            </tr>
            <tr>
                <th class="caption_table">Nuevos</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class=""> $4,365.60 </td>
            </tr>
            <tr>
                <th class="caption_table">Usados</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">Ordenes de Reparación</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <td class="title_table" colspan="14"><b> Ventas </b></td>
            </tr>
            <tr>
                <th class="caption_table">Nuevos</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">Semi Nuevos</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>

            <tr>
                <th class="caption_table">Servicio</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">Refacciones</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">Hojalateria y pintura</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table"><b>Total Ventas</b></th>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
            </tr>

            <tr>
                <td class="title_table" colspan="14"><b> Utilidad bruta </b></td>
            </tr>
            <tr>
                <th class="caption_table">Vehiculos</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class=""> $4,365.60 </td>
            </tr>
            <tr>
                <th class="caption_table">Semi nuevos</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>

            </tr>
            <tr>
                <th class="caption_table">Servicio</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>

            </tr>
            <tr>
                <th class="caption_table">Refacciones</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>

            </tr>
            <tr>
                <th class="caption_table">Hojalateria y pintura</th>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
                <td class="">$0.00 </td>
            </tr>
            <tr>
                <th class="caption_table">Total</th>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
                <td class="text-warning bold">$0.00 </td>
            </tr>
            <tr colspan="11">
                <th>Gastos</th>
            </tr>
            <tr>
                <td class="caption_table"> Sueldos y Comisiones</td>
                <th class="">$0.00</th>

                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Ventas Nuevos</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Ventas Semi Nuevos</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Refacciones y Servicio</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Administración</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Prestaciones</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Uniformes</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Limpieza y Vigilancia</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>

            </tr>
            <tr>

                <td class="caption_table"> Subsidios U. Nuevas</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Publicidad</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Promoción</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>

            </tr>
            <tr>

                <td class="caption_table"> Traslado Unidades</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Almacenamiento</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Conauto</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Gasolina y Lubricantes</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Teléfono</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Gastos de Viaje</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Herramientas</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Entrenamiento</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Servicio Gratuito</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Fletes</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Impuestos</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Depreciación Activos</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Mantenimiento</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Renta</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Agua y Luz</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Seguros y Fianzas</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Honorarios</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Administración</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Cuotas y Donativos</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>

                <td class="caption_table"> Sistemas</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>


            </tr>
            <tr>
                <td class="caption_table"> Papelería</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
            </tr>
            <tr>
                <td class="caption_table"> Otros</td>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
                <th class="">$0.00</th>
            </tr>
            <tr>
                <td class="caption_table"> Total gastos</td>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
            </tr>
            <tr>
                <td class="caption_table"> <b>Utilidad de Operación</b></td>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
                <th class="text-warning bold">$0.00</th>
            </tr>
        </tbody>
    </table>


@endsection
