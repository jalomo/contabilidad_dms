@layout('layout')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('contenido')
<h1 class="h3 mb-4 text-gray-800">
    <?php echo $poliza->fecha; ?>&nbsp&nbsp
    <?php echo $poliza->poliza; ?>&nbsp&nbsp
    <?php echo $poliza->concepto; ?>
</h1>
<form id="guardar_formulario">
    <div class="row">
        <div class="col-4">
            <div class="form-group">
                <label for="">Cuenta</label>
                <select name="cuenta" id="cuenta" class="form-control">
                    <?php foreach ($cuentas as $row) : ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->cuenta . "-" . $row->decripcion; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="">Nombre</label>
                <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="">Referencia</label>
                <input type="text" class="form-control" id="referencia" placeholder="Referencia" name="referencia">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="">Concepto</label>
                <input type="text" class="form-control" id="concepto" placeholder="Concepto" name="concepto">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="">Cargo</label>
                <input type="text" class="form-control" id="cargo" placeholder="Cargo" name="cargo" value="0">
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <label for="">Abono</label>
                <input type="text" class="form-control" id="abono" placeholder="Abono" name="abono" value="0">
            </div>
        </div>
    </div>
    <div class="text-right mb-5">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Guardar</button>
    </div>
</form>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Cuenta</th>
            <th>Nombre</th>
            <th>Referencia</th>
            <th>Concepto</th>
            <th>Cargo</th>
            <th>Abono</th>
            <th>documentos</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($asientos as $row_asiento) : ?>
            <tr>
                <th><?php echo nombre_cuenta($row_asiento->cuenta); ?></th>
                <th><?php echo $row_asiento->nombre; ?></th>
                <th><?php echo $row_asiento->referencia; ?></th>
                <th><?php echo $row_asiento->concepto; ?></th>
                <th><?php echo $row_asiento->cargo; ?></th>
                <th><?php echo $row_asiento->abono; ?></th>
                <th>
                    <button type="button" class="btn btn-primary">Documentos</button>
                </th>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $("#dataTable").DataTable({
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
            }
        });
        $('#guardar_formulario').submit(function(event) {
            event.preventDefault();
            var url = "<?php echo base_url() ?>index.php/asientos/guarda/<?php echo $id_id ?>";
            ajaxJson(url, {
                    "cuenta": $("#cuenta").val(),
                    "nombre": $("#nombre").val(),
                    "concepto": $("#concepto").val(),
                    "referencia": $("#referencia").val(),
                    "abono": $("#abono").val(),
                    "cargo": $("#cargo").val()
                },
                "POST", "",
                function(result) {
                    correoValido = false;
                    console.log(result);
                    json_response = JSON.parse(result);
                    obj_output = json_response.output;
                    obj_status = obj_output.status;
                    if (obj_status == false) {
                        aux = "";
                        $.each(obj_output.errors, function(key, value) {
                            aux += value + "<br/>";
                        });
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                    }
                    if (obj_status == true) {
                        exito_redirect(" GUARDADO CON EXITO", "success", "<?php echo base_url() ?>index.php/asientos/ver/<?php echo $id_id ?>");
                    }
                });
        });
    });
</script>
@endsection