<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asientos extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
       
    }

    public function ver($id_id){
        $datos['poliza'] = $this->Mgeneral->get_row('id_id',$id_id,'polizas');
        $datos['index'] = '';
        $datos['cuentas'] = $this->Mgeneral->get_table('cuentas_dms');
        $datos['asientos'] = $this->Mgeneral->get_result('poliza_id_id',$id_id,'asientos');
        $datos['id_id'] = $id_id;
        
        $this->blade->render('alta',$datos);
    }

    public function guarda($id_id){
        $this->form_validation->set_rules('cuenta', 'cuenta', 'required');
        $this->form_validation->set_rules('nombre', 'nombre', 'required');
        $this->form_validation->set_rules('referencia', 'referencia', 'required');
        $this->form_validation->set_rules('concepto', 'concepto', 'required');
        $this->form_validation->set_rules('cargo', 'cargo', 'required');
        $this->form_validation->set_rules('abono', 'abono', 'required');
  
        $response = validate($this);
  
  
        if($response['status']){
          $data['cuenta'] = $this->input->post('cuenta');
          $data['nombre'] = $this->input->post('nombre');
          $data['referencia'] = $this->input->post('referencia');
          $data['concepto'] = $this->input->post('concepto');
          $data['cargo'] = $this->input->post('cargo');
          $data['abono'] = $this->input->post('abono');
          $data['id_id'] = get_guid();
          $data['poliza_id_id'] = $id_id;
          
  
          $this->Mgeneral->save_register('asientos', $data);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));
    }

    

    
  
}