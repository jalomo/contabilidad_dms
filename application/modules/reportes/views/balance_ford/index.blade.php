@layout('layout')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Mylsa Querétaro, S.A. DE C.V. Y Compañias Afiliadas
    <br/>
    Balances Generales Individuales y Combinados 
</h4>

<?php if(isset($table)){ ?>
<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

                <h4 class="card-title "><b>Balance al 14 De Septiembre del 2021</b></h4>
                <p class="card-text">
                    <div class="table-responsive">
                        <?php echo $table; ?>
                    </div>
                </p>
            </div>
        </div>
    </div>
</div>
<?php } ?>

@endsection

@section('style')
<style>

    .card-body {
        background-color: white;
    }

    #content {
        background: #f3f4f5 !important;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
</style>
@endsection