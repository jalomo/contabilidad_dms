<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Balance_consejo extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));
        date_default_timezone_set('America/Mexico_City');
       
    }

    public function index(){
       
        $this->load->model('CaBalanceConsejo_model');
        $balanza = $this->CaBalanceConsejo_model->getList();

        $this->load->library('table');
        $template = array(
            'table_open'            => '<table border="0" class="table table-bordered">',
        );
        $this->table->set_template($template);

        $cell_h1 = array('data' => 'ACTIVO', 'class' => 'highlight', 'colspan' => 2);
        $cell_h2 = array('data' => 'PASIVO', 'class' => 'highlight', 'colspan' => 2);

        $this->table->set_heading($cell_h1,$cell_h2);
        if(is_array($balanza)){
            foreach ($balanza as $key => $value) {
                $f2 = trim($value['f2']);
                $f3 = strlen(trim($value['f3']))>0 ? (($value['f3'] <> 0)? '$ '.utils::formatMoney(trim($value['f3'])) : '<b>-</b>') : '';
                $f4 = strlen(trim($value['f4']))>0 ? (($value['f4'] <> 0)? '$ '.utils::formatMoney(trim($value['f4'])) : '<b>-</b>') : '';

                $f6 = trim($value['f6']);
                $f7 = strlen(trim($value['f7']))>0 ? (($value['f7'] <> 0)? '$ '.utils::formatMoney(trim($value['f7'])) : '<b>-</b>') : '';

                $row = array(
                    2 => $f2,
                    3 => $f3,
                    
                    6 => $f6,
                    7 => $f7,
                );

                if (preg_match("/total/i", $f2)) {
                    $row[2] = array('data' => $f2, 'class' => 'text-right');
                }

                if (preg_match("/total/i", $f6)) {
                    $row[6] = array('data' => $f6, 'class' => 'text-right');
                }

                if (preg_match("/suma/i", $f2)) {
                    $row[2] = array('data' => $f2, 'class' => 'font-weight-bold text-right');
                    $row[3] = array('data' => $f3, 'class' => 'font-weight-bold');
                    // $row[4] = array('data' => $f4, 'class' => 'font-weight-bold');
                }

                if (preg_match("/suma/i", $f6)) {
                    $row[6] = array('data' => $f6, 'class' => 'font-weight-bold text-right');
                    $row[7] = array('data' => $f7, 'class' => 'font-weight-bold');
                }

                if( $f2 != '' && $f3 == ''  ){
                    $row[2]= array('data' => $f2, 'class' => 'text-center','colspan' => 2);
                    unset($row[3]);
                    // unset($row[4]);
                }
                
                if( $f6 != '' && $f7 == '' ){
                    $row[6]= array('data' => $f6, 'class' => 'text-center','colspan' => 2);
                    unset($row[7]);
                }

                $this->table->add_row($row);
                
                
            }
        }

        $data_content = array(
            'table' => $this->table->generate()
        );
        
        $this->blade->render('/balance_consejo/index',$data_content);
    }
}