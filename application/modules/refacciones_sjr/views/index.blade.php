@layout('layout')

@section('contenido')
<style>
    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
    }
</style>

<h4 class="mt-4 mb-4 text-gray-800 text-center">
    REFACCIONES SAN JUAN DEL RIO<br />
    RESULTADOS DE OPERACIÓN
</h4>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="title_table"></th>
            <th class="caption_table">ENE.20</th>
            <th class="caption_table">FEB.20</th>
            <th class="caption_table">MZO. 20</th>
            <th class="caption_table">ABR.20</th>
            <th class="caption_table">MAY.20</th>
            <th class="caption_table">JUN.20</th>
            <th class="caption_table">JUL.20</th>
            <th class="caption_table">AGO.20</th>
            <th class="caption_table">SEPT.20</th>
            <th class="caption_table">OCT.20</th>
            <th class="caption_table">NOV.20</th>
            <th class="caption_table">DIC.20</th>
            <th class="caption_table">TOTAL</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="title_table" colspan="14"><b>VENTAS</b></td>
        </tr>
        <?php foreach ($ventas as $venta) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $venta[$caption] : utils::toMoney($venta[$caption]); ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"></td>
        </tr>
        <?php foreach ($total_ventas as $total_venta) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $total_venta[$caption] : '<span style="font-weight:bold" class="text-success">' . utils::toMoney($total_venta[$caption]) . '</span>'; ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"><b>COSTOS DE VENTAS</b></td>
        </tr>
        <?php foreach ($costos_ventas as $costo) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $costo[$caption] : utils::toMoney($costo[$caption]); ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"></td>
        </tr>
        <?php foreach ($total_costo_ventas as $total_costo_venta) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $total_costo_venta[$caption] : '<span style="font-weight:bold" class="text-warning">' . utils::toMoney($total_costo_venta[$caption]) . '</span>'; ?></td>
                <?php } ?>
            </tr>
        <?php } ?>

        <tr>
            <td class="title_table" colspan="14"></td>
        </tr>
        <?php foreach ($utilidades as $utilidad) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $utilidad[$caption] : '<span style="font-weight:bold">'. utils::toMoney($utilidad[$caption]) . '</span>' ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <?php
        foreach ($porcentajes as $porcentaje) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ?  $porcentaje[$caption] :'<span style="font-weight:bold">'. $porcentaje[$caption] . '% </span>'; ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"><b>GASTOS DE VENTA:</b></td>
        </tr>
        <tr>
            <td class="title_table" colspan="1"><b>SUELDO Y COMIS. GERENTE:</b></td>
            <td class="" colspan="13"><b></b></td>
        </tr>
        <?php foreach ($sueldos_comisiones as $comision) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $comision[$caption] : ($comision[$caption]); ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"></td>
        </tr>
        <?php foreach ($porcentaje_sobre_ventas as $val) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $val[$caption] : ($val[$caption]); ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"><b>UTILIDAD DEPARTAMENTAL </b></td>
        </tr>
        <?php foreach ($antes_transferencia as $val) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $val[$caption] : ($val[$caption]); ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"><b>TRANSF. DE UTILIDAD</b></td>
        </tr>
        <?php foreach ($transferencias as $val) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $val[$caption] : ($val[$caption]); ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"></td>
        </tr>
        <?php foreach ($utilidad_ajustes_porcentajes as $val) { ?>
            <tr>
                <?php foreach ($captions as $caption) { ?>
                    <td <?php echo $caption == 'nombre' ? 'class="caption_table"' : ''; ?>><?php echo $caption == "nombre" ? $val[$caption] : ($val[$caption]); ?></td>
                <?php } ?>
            </tr>
        <?php } ?>



    </tbody>
</table>


@endsection