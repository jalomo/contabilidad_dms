@layout('layout')

@section('contenido')
<style>
    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
    }
    .bold {
        font-weight: bold;
    }
</style>

<h4 class="mt-4 mb-4 text-gray-800 text-center">
    REFACCIONES SAN JUAN DEL RIO<br />
    RESULTADOS DE OPERACIÓN
</h4>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th class="title_table"></th>
            <th class="caption_table">ENE.20</th>
            <th class="caption_table">FEB.20</th>
            <th class="caption_table">MZO. 20</th>
            <th class="caption_table">ABR.20</th>
            <th class="caption_table">MAY.20</th>
            <th class="caption_table">JUN.20</th>
            <th class="caption_table">JUL.20</th>
            <th class="caption_table">AGO.20</th>
            <th class="caption_table">SEPT.20</th>
            <th class="caption_table">OCT.20</th>
            <th class="caption_table">NOV.20</th>
            <th class="caption_table">DIC.20</th>
            <th class="caption_table">TOTAL</th>
        </tr>
    </thead>
    <tbody>

        <?php 
        $tipo = '';
        foreach ($data as $val) { ?>
            @if ($val['tipo'] != $tipo)
            <?php  $tipo = $val['tipo'] ?>
            <tr>
                <td class="title_table" colspan="14"><b> {{ $val['tipo'] }} </b></td>
            </tr>
            @endif
            <tr>
                <td class="caption_table">
                    {{ $val['nombre'] }}
                </td>
                @if ($val['tipo_porcentaje'] != true)
                    @for($i=1; $i<=12; $i++)
                        <td class="{{ $val['class'] }}">{{ utils::toMoney($val['mes_'.$i]) }} </td>
                    @endfor
                    <td  class="{{ $val['class'] }}"> {{ utils::toMoney($val['total']) }} </td>
                @else
                    @for($i=1; $i<=12; $i++)
                        <td class="{{ $val['class'] }}">{{ $val['mes_'.$i]. '%' }} </td>
                    @endfor
                    <td  class="{{ $val['class'] }}"> {{ $val['total']. '%' }} </td>
                @endif

            </tr>
        <?php } ?>
        <tr>
            <td class="title_table" colspan="14"></td>
        </tr>

    </tbody>
</table>


@endsection