@layout('layout')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('contenido')
<h1 class="h3 text-gray-800">
    Cuentas
</h1>
<div class="text-right mb-5">
    <a class="btn btn-primary" href="<?php echo site_url('cuentas/alta_cuenta'); ?>"><i class="fa fa-plus"></i> &nbsp;Alta cuenta</a>
</div>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead class="title_table">
        <tr>
            <th>Cuenta</th>
            <th>Nombre</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Cuenta</th>
            <th>Nombre</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($cuentas as $row) : ?>
            <tr>
                <th><?php echo $row->cuenta; ?></th>
                <th><?php echo $row->decripcion; ?></th>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $("#dataTable").DataTable({
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
            }
        });
    });
</script>
@endsection