@layout('layout')

@section('style')
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
</style>
@endsection

@section('contenido')

<script>
    const identity = "<?php echo $id; ?>";
</script>
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    {{ isset($reporte['nombre']) ? $reporte['nombre'] : ''}}
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-light bg-light">
                            <form class="form-inline" id="form_busqueda">
                                <span class="navbar-text mr-2">
                                    <b>Seleccione el año : </b>
                                </span>
                                <input class="form-control mr-sm-2" type="number" name="anio" placeholder="" aria-label="" value="<?php echo date('Y'); ?>" min="2020" max="<?php echo date('Y'); ?>">
                                <small id="msg_fecha" class="form-text text-danger"></small>
                                <input type="hidden" name="reporte_id" value="{{ isset($reporte['id']) ? base64_encode($reporte['id']) : ''}}"/>
                                <button class="btn btn-outline-success ml-2 my-2 my-sm-0" onclick="buscar();" type="button"><i class="fa fa-search"></i> Buscar</button>
                            </form>
                        </nav>
                    </div>
                </div>
                <div class="contenido_general  mt-5">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.2.1/mustache.min.js" integrity="sha512-Qjrukx28QnvFWISw9y4wCB0kTB/ISnWXPz5/RME5o8OlZqllWygc1AB64dOBlngeTeStmYmNTNcM6kfEjUdnnQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    function zfill(number, width) {
        var numberOutput = Math.abs(number);
        var length = number.toString().length;
        var zero = "0";

        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString());
            } else {
                return numberOutput.toString();
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString());
            }
        }
    }

    function generar() {
        $('small.form-text.text-danger').html('');
        $('div.contenido_general').html('');
        Pace.track(function() {
            $.ajax({
                type: 'POST',
                url: PATH + '/estados_financieros/api/apis_generar_contenido/construir_reporte',
                data: {
                    anio: $('input[name=anio]').val(),
                    reporte_id: identity
                },
                dataType: "json",
                traditional: true,
                complete: function(data) {
                    buscar();
                }
            });
        });
    }

    function buscar() {
        $('small.form-text.text-danger').html('');
        $('div.contenido_general').html('');
        Pace.track(function() {
            $.ajax({
                type: 'POST',
                url: PATH + '/estados_financieros/api/apis_construir_contenido/get_contenido_reporte',
                data: {
                    anio: $('input[name=anio]').val(),
                    reporte_id: identity
                },
                dataType: "json",
                traditional: true,
                success: function(data) {
                    if (data.error == true) {
                        $.each(data.mensaje, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }
                        });
                    } else {
                        let listado = atob(data.data.tabla);
                        $('div.contenido_general').html(listado);
                    }
                }
            });
        });
    }


    $(function() {
        Pace.stop();
        moment.locale('es');
        generar();
    })
</script>
@endsection