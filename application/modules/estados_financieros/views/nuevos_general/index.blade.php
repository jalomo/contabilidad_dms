@layout('layout')

@section('style')
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
    
</style>
@endsection

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    NUEVOS GENERAL														
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

               

                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-light bg-light">
                            <form class="form-inline" id="form_busqueda">
                                <span class="navbar-text mr-2">
                                    <b>Fecha: </b>
                                </span>
                                <input class="form-control mr-sm-2" type="number" name="anio" placeholder=""
                                    aria-label="" value="<?php echo date('Y'); ?>" min="2020" max="<?php echo date('Y'); ?>">
                                    <small id="msg_fecha" class="form-text text-danger"></small>
                                <button class="btn btn-outline-success ml-2 my-2 my-sm-0" onclick="buscar();" type="button">Buscar</button>
                            </form>
                        </nav>
                    </div>
                </div>
                <div class="contenido_general  mt-5">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.2.1/mustache.min.js" integrity="sha512-Qjrukx28QnvFWISw9y4wCB0kTB/ISnWXPz5/RME5o8OlZqllWygc1AB64dOBlngeTeStmYmNTNcM6kfEjUdnnQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

    function zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString()); 
            } else {
                return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    
    function obtener_numero_semana(mJsDate){
        if(mJsDate.isValid()){
            if($.inArray(mJsDate.isoWeekday(),[6,7]) == '-1' ){
                return  mJsDate.format('WW');
            }else{
                return null;
            }
                    
        }else{
            return null;
        }
    }

    function obtener_dias(dayStr,endOfMonth){
        var dias = [];
        dias.push(dayStr.format('Y-MM-DD'));
        for (let index = dayStr.isoWeekday(); index < 5; index++) {
            dayStr = dayStr.add(1, 'days');
            if(dayStr <= endOfMonth){
                dias.push(dayStr.format('Y-MM-DD'));
            }
        }
        return dias;
    }

    function buscar(){

        $('small.form-text.text-danger').html('');
        $('div.contenido_general').html('');

        Pace.track(function() {
            $.ajax({
                type: 'POST',
                url: PATH+'/estados_financieros/api/apis_nuevos_general/get_contenido_reporte',
                data: $('form#form_busqueda').serializeArray(),
                dataType: "json",
                traditional: true,
                success: function (data) {
                    if(data.error == true){
                        $.each(data.mensaje, function( index, value ) {
                            if( $('small#msg_'+index).length ){
                                $('small#msg_'+index).html(value);
                            }
                        });
                    } else {
                        let listado = atob(data.data.tabla);
                        $('div.contenido_general').html(listado);
                    } 
                                
                }
            });
        });
    }
    

    $(function(){

        Pace.stop();

        moment.locale('es');
        buscar();

    })
</script>
@endsection