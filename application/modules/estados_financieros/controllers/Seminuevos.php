<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Seminuevos extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){ 
        $data = [
            'listado' => ''
        ];
        $this->blade->render('/seminuevos/index',$data);
    }
}
