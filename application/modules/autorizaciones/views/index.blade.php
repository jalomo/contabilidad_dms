@layout('layout')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
</style>
@endsection

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    AUTORIZACIÓN DE CREDITO CLIENTES
</h4>

<div class="card mt-3">
    <div class="card-body">
        <div class="row ">
            <div class="col-12">
                <table class="table table-striped" id="clientes_dms" width="100%" cellspacing="0">
                </table>
            </div>
        </div>

    </div>
</div>

<div class="modal" id="modal_autorizacion" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Autorizar plazo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label><b>* Plazo credito</b></label>
                    <select name="plazo_credito_id" class="form-control" id="plazo_credito_id">
                        <option value="">Seleccion una opción</option>
                    </select>
                </div>
                <div class="form-group">
                    <label><b>* Límite de credito</b></label>
                    <input type="text" name="limite_credito" class="form-control money_format" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="app.autorizaPlazo()" class="btn btn-primary">Aplicar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.2/jquery.mask.min.js" referrerpolicy="no-referrer"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    var app;
    var table = 'table#clientes_dms';
    var cliente_id = '';

    class Funciones {
        create_table() {
            $(table).DataTable(this.settings_table());
        }

        settings_table() {
            return {
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
                },
                columns: [{
                        title: 'Número cliente',
                        data: function(data) {
                            return data.numero_cliente
                        }
                    },
                    {
                        title: 'Razón social',
                        data: 'nombre_empresa'

                    },
                    {
                        title: 'Nombre cliente',
                        data: function(data) {
                            return data.nombre + data.apellido_paterno + data.apellido_materno
                        }
                    },
                    {
                        title: 'RFC',
                        data: 'rfc'
                    },
                    {
                        title: 'Teléfono',
                        data: 'telefono',
                    },
                    {
                        title: 'Correo electrónico',
                        data: 'correo_electronico',
                    },
                    {
                        title: 'Tiene crédito',
                        data: 'aplica_credito',
                        render: function(data, type, row) {
                            return data && data == true ? 'Si' : 'No';
                        }
                    },
                    {
                        title: 'Plazo crédito',
                        data: 'plazo_credito',
                        render: function(data, type, row) {
                            return data && data.id ? data.nombre : 'Sin plazo';
                        }
                    },
                    {
                        title: 'Límte crédito',
                        data: 'limite_credito',
                        render: function(data, type, row) {
                            return data ? '<span class="money_format">' + data + '</span>' : '';
                        }
                    },
                    {
                        title: 'Acciones',
                        data: 'aplica_credito',
                        width: '120px',
                        render: function(data, type, row) {
                            // let aplica_credito = '<button title="Autorizar credito" data-cliente_id= "' + row.id + '" data-autoriza="1" onclick="app.Autoriza_credito(this)" class="btn btn-success"><i class="fas fa-check"></i></button>';
                            let aplica_credito = '';
                            let autoriza_plazo = '';
                            if (data && data == true) {
                                aplica_credito = '<button title="Eliminar credito" data-cliente_id= "' + row.id + '" data-autoriza="2" onclick="app.Autoriza_credito(this)" class="btn btn-danger"><i class="fas fa-times"></i></button>';
                            } else {
                                autoriza_plazo = '<button title="Autorizar Plazo Credito" data-cliente_id= "' + row.id + '" data-tipo_plazo_credito="' + row.plazo_credito_id + '" onclick="app.OpenModalPlazo(this)" class="btn btn-primary"><i class="fas fa-tag"></i></button>';
                            }
                            return aplica_credito + ' ' + autoriza_plazo;
                        }

                    },
                ]
            }
        }

        Autoriza_credito(_this) {
            let data = {
                "aplica_credito": $(_this).data('autoriza') == 1 ? 1 : 0,
            };

            $.ajax({
                url: API_URL_DMS + 'clientes/actualizaCredito/' + $(_this).data('cliente_id'),
                type: 'PUT',
                data: data,
                success: function(data) {
                    if (data) {
                        let mensaje = $(_this).data('autoriza') == 1 ? 'Credito autorizado correctamente' : 'Credito eliminado correctamente';
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito',
                            text: mensaje,
                        });
                        app.get_busqueda();
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Ocurrio un error inesperado ...',
                        })
                    }
                }
            });
        }

        autorizaPlazo(_this) {
            if ($("select[name*='plazo_credito_id']").val() < 1) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Favor de elegir un plazo de crédito',
                })
            } else if (!$("input[name*='limite_credito']").val().length > 1) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Favor de elegir el limite de crédito',
                })
            }
            let limite_credito = $("input[name*='limite_credito']").val();
            limite_credito = limite_credito.replace(",", "");
            $.ajax({
                url: API_URL_DMS + 'clientes/actualizaPlazoCredito/' + cliente_id,
                type: 'PUT',
                data: {
                    plazo_credito_id: $("select[name*='plazo_credito_id']").val(),
                    limite_credito: limite_credito
                },
                success: function(data) {
                    if (data) {
                        let mensaje = 'Datos de credito actualizado correctamente';
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito',
                            text: mensaje,
                        });
                        $("#modal_autorizacion").modal("hide");
                        app.get_busqueda();
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Ocurrio un error inesperado ...',
                        })
                    }
                },
                error: function(respuesta) {
                    console.log(respuesta);
                }
            });
        }

        get_busqueda() {
            return new Promise(function(resolve, reject) {
                $.ajax({
                    dataType: "json",
                    type: 'GET',
                    dataSrc: "",
                    url: API_URL_DMS + 'clientes/',
                    success: function(response) {
                        let listado = $(table).DataTable();
                        listado.clear().draw();
                        if (response && response.length > 0) {
                            response.forEach(listado.row.add);
                            listado.draw();
                            resolve(response);
                        }
                    }
                });
            })
        }

        OpenModalPlazo(_this) {
            cliente_id = $(_this).data('cliente_id');
            $("#modal_autorizacion").modal("show");
            $("select[name*='plazo_credito_id']").val('');
            $("input[name*='limite_credito']").val('');
            setTimeout(() => {
                $.ajax({
                    dataType: "json",
                    type: 'GET',
                    dataSrc: "",
                    url: API_URL_DMS + 'catalogo-plazo-credito/',
                    success: function(response) {
                        var listitems = '';
                        var selecciona = $(_this).data("tipo_plazo_credito");
                        $.each(response, function(key, value) {
                            if (selecciona == value.id) {
                                listitems += '<option data-cantidad_mes=' + value.cantidad_mes + ' selected value=' + value.id + '>' + value.nombre + '</option>';
                            } else {
                                listitems += '<option data-cantidad_mes=' + value.cantidad_mes + ' value=' + value.id + '>' + value.nombre + '</option>';
                            }

                        });
                        $("select[name*='plazo_credito_id']").append(listitems);
                        if (!selecciona) {
                            $("select[name*='plazo_credito_id']").val('');
                        }
                    }
                });
            }, 1000);

        }

    }

    $(function() {
        app = new Funciones();
        app.create_table();
        app.get_busqueda().then(x => {
            $('.money_format').mask("#,##0.00", {
                reverse: true
            });
        })
    });
</script>
@endsection