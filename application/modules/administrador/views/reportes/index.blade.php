@layout('layout')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    ADMINISTRADOR DE REPORTES
</h4>

<div class="card mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="text-right">
                    <button type="button" data-tipo="alta" onclick="app.open_modal_reporte(this)" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Añadir reporte</button>
                </div>
                <table class="table table-striped" id="reportes" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_reporte" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">-</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label><b>* Nombre reporte</b></label>
                    <input type="text" id="reporte_nombre" name="reporte_nombre" class="form-control" />
                    <small id="msg_reporte_nombre" class="form-text text-danger"></small>
                </div>
            </div>
            <input type="hidden" name="reporte_id" id="reporte_id" value="" />
            <div class="modal-footer">
                <button type="button" onclick="app.guardar_reporte()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    var app;
    var table = 'table#reportes';
    var cliente_id = '';

    class Funciones {
        create_table() {
            $(table).DataTable(this.settings_table());
        }
        settings_table() {
            return {
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
                },
                columns: [{
                        title: 'id',
                        data: 'id',
                        type: 'num'
                    },
                    {
                        title: 'Reporte',
                        data: 'nombre'
                    },
                    {
                        title: 'Fecha registro',
                        data: 'created_at'
                    },
                    {
                        title: 'Acciones',
                        width: '140px',
                        render: function(data, type, row) {
                            let editar = '';
                            let detalle = '';
                            let eliminar = '';
                            editar = '<button title="Editar reporte" data-reporte_id= "' + row.id + '" data-tipo="editar" data-nombre = "' + row.nombre + '"  onclick="app.open_modal_reporte(this)" class="btn btn-dark"><i class="fas fa-pencil-alt"></i></button>';
                            detalle = '<button title="Contenido Reporte" data-reporte_id= "' + row.id + '"  onclick="app.contenido_reporte(this)" class="btn btn-primary"><i class="fas fa-file-excel"></i></button>';
                            eliminar = '<button title="Eliminar reporte" data-reporte_id= "' + row.id + '" data-tipo="eliminar" data-nombre = "' + row.nombre + '"  onclick="app.eliminar(this)" class="btn btn-danger"><i class="fas fa-trash"></i></button>';

                            return editar + ' ' + detalle + ' ' + eliminar;
                        }

                    },
                ]
            }
        }
        contenido_reporte(_this) {
            window.location.href = PATH + '/administrador/reportes/contenido?reporte_id=' + $(_this).data('reporte_id')
        }
        get_busqueda() {
            $.ajax({
                dataType: "json",
                type: 'GET',
                dataSrc: "",
                url: PATH + '/administrador/api/reportes/listado',
                success: function(response) {
                    let listado = $(table).DataTable();
                    listado.clear().draw();
                    if (response.data && response.data.length > 0) {
                        response.data.forEach(listado.row.add);
                        listado.draw();
                    }
                }
            });
        }
        eliminar(_this) {
            Swal.fire({
                title: 'Desea eliminar el reporte?',
                showDenyButton: true,
                showCancelButton: false,
                confirmButtonText: 'Aceptar',
                denyButtonText: 'Cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: PATH + '/administrador/api/reportes/delete_reporte',
                        type: 'POST',
                        data: {
                            reporte_id: $(_this).data('reporte_id'),
                        },
                        success: function(response) {
                            if (response.estatus != "error") {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Éxito!',
                                    text: response.mensaje,
                                }).then((result) => {
                                    app.get_busqueda();
                                });

                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: response.mensaje,
                                })
                            }
                        },
                    });
                } else {
                    return false;
                }
            })
        }
        open_modal_reporte(_this) {
            let tipo = $(_this).data('tipo');
            $("input[name*='reporte_nombre']").val('');
            $("input[name*='reporte_id']").val('');

            if (tipo == 'alta') {
                $(".modal-title").html('Alta Reporte');
            } else {
                $(".modal-title").html('Editar Reporte');
                $("input[name*='reporte_nombre']").val($(_this).data('nombre'));
                $("input[name*='reporte_id']").val($(_this).data('reporte_id'));
            }
            $("#modal_reporte").modal("show");

        }
        guardar_reporte(_this) {
            if (!$("input[name*='reporte_nombre']").val().length > 1) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'El nombre del reporte es obligatorio',
                })
            }
            $.ajax({
                url: PATH + '/administrador/api/reportes/save_reporte',
                type: 'POST',
                data: {
                    reporte_id: $("input[name*='reporte_id']").val(),
                    reporte_nombre: $("input[name*='reporte_nombre']").val()
                },
                success: function(response) {
                    if (response.estatus != "error") {
                        $("#modal_reporte").modal("hide");
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito!',
                            text: response.mensaje,
                        }).then((result) => {
                            app.get_busqueda();
                        });

                    } else {
                        if (response.errores) {
                            $.each(response.errores, function(index, value) {
                                if ($("small#msg_" + index).length) {
                                    $("small#msg_" + index).html(value);
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.mensaje,
                            })
                        }
                    }
                },
                error: function(respuesta) {
                    console.log(respuesta);
                }
            });
        }

    }

    $(function() {
        app = new Funciones();
        app.create_table();
        app.get_busqueda();
    });
</script>
@endsection