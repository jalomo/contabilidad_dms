@layout('layout')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<style>
hr.style-six {
    border: 0;
    height: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}

table.dataTable td.details-control:before {
   content: '\f152';
   font-family: 'Font Awesome\ 5 Free';
   cursor: pointer;
   font-size: 22px;
   color: #55a4be;
}
table.dataTable tr.shown td.details-control:before {
  content: '\f150';
  color: black;
}

</style>
@endsection

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    ADMINISTRAR REPORTE
</h4>


<div class="card mt-3">
    <div class="card-body" id="principal_tabla">

        <div class="row">

            <div class="col-sm-12">
                <div class="form-group row">
                    <label for="form_nombre_reporte" class="col-sm-2 col-form-label">Nombre del reporte:</label>
                    <div class="col-sm-10">
                        <div class="input-group mb-3">
                            <input type="hidden" id="reporte_id" value="{{ isset($datos['id']) ? $datos['id'] : '' }}" />
                            <input type="text" readonly id="form_nombre_reporte" name="form_nombre_reporte" class="form-control" aria-describedby="button-addon2" value="{{ isset($datos['nombre']) ? $datos['nombre'] :  '' }}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2" data-toggle="tooltip" data-placement="top" title="Editar" onclick="ctrl_contenido.open_modal_reporte();" ><i class="fas fa-pencil-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>  
                <hr class="style-six">
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-5">
                        <h5 class="card-title">Contenido</h5>
                    </div>
                    <div class="col-7">
                        <div class="text-right">
                            <button type="button" data-renglon_id="" onclick="app.open_modal_cuenta()" class="btn btn-primary btn-sm mb-4"><i class="fas fa-plus"></i> Añadir renglón</button>
                        </div>
                    </div>
                </div>
                
                

                <table class="table table-striped table-sm" id="contenido_reporte" width="100%" cellspacing="0">
                </table>

            </div>
        </div>
        <a href="{{ site_url('administrador/reportes/index') }}" class="btn btn-dark mt-4 mb-3"><i class="fa fa-arrow-left"></i> Regresar</a>

    </div>
    <div class="card-body" id="operaciones_tabla" style="display:none">
        <h3>Operacion cuenta <span id="nombre_cuenta_"></span></h3>
        <div class="row">
            <div class="col-12">
                <div class="text-right">
                    <button type="button" data-renglon_id="" onclick="app.open_modal_operacion()" class="btn btn-primary mb-4"><i class="fa fa-plus-circle"></i> Añadir operacion</button>
                </div>
                <table class="table table-striped" id="operaciones" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
        <button type="button" onclick="app.regresar_principal()" class="btn btn-dark mt-4 mb-3"><i class="fa fa-arrow-left"></i> Regresar</button>
    </div>
</div>


<!-------------------------------------------------------------------------------------------------------------- -->
<div class="modal" id="modal_reporte" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar reporte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label><b>* Nombre reporte</b></label>
                    <input type="text" id="reporte_nombre" name="reporte_nombre" class="form-control" />
                    <small id="msg_reporte_nombre" class="form-text text-danger"></small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="ctrl_contenido.guardar_nombre_reporte()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-------------------------------------------------------------------------------------------------------------- -->


<!-------------------------------------------------------------------------------------------------------------- -->
<!-- MODAL CUENTAS -->
<!-------------------------------------------------------------------------------------------------------------- -->
<div class="modal" id="modal_cuenta" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">-</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modal_cuentas">
                    <div class="form-group">
                        <label><b>* Nombre renglón</b></label>
                        <input type="text" id="nombre_cuenta" name="nombre_cuenta" class="form-control" />
                        <small id="msg_nombre_cuenta" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label><b>* Tipo renglón</b></label>
                        <select name="tipo_renglon" id="tipo_renglon" onchange="app.actualizar_tipo_renglon()" class="custom-select mb-3">
                            <option value="1">Operación</option>
                            <option value="3">Título</option>
                            <option value="2">Subtitulo</option>
                        </select>
                        <small id="msg_tipo_renglon" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group" id="div_tipo_formato" style="display:none;">
                        <label><b> Tipo formato</b></label>
                        <select name="tipo_formato" id="tipo_formato" class="custom-select">
                            <option value="moneda">Moneda</option>
                            <option value="porcentaje">Porcentaje</option>
                            <option value="decimal">Decimal</option>
                        </select>
                        <small id="msg_tipo_formato" class="form-text text-danger"></small>
                    </div>

                    <input type="hidden" name="renglon_id" id="renglon_id" value="" />

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" onclick="app.guardar_cuenta()" class="btn btn-primary"> Guardar</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-------------------------------------------------------------------------------------------------------------- -->


<!-------------------------------------------------------------------------------------------------------------- -->
<div class="modal " id="modal_operaciones" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">-</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            </div>
            <input type="hidden" name="operacion_id_" id="operacion_id_" value="" />
            <div class="modal-footer">
                <button type="button" onclick="app.guardar_operacion()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-------------------------------------------------------------------------------------------------------------- -->


<!-------------------------------------------------------------------------------------------------------------- -->
<div class="modal" id="modal_fomulas_reglon" tabindex="-1" role="dialog">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Operaciones del reglón</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-------------------------------------------------------------------------------------------------------------- -->

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js" integrity="sha512-BmM0/BQlqh02wuK5Gz9yrbe7VyIVwOzD1o40yi1IsTjriX/NGF37NyXHfmFzIlMmoSIBXgqDiG1VNU6kB5dBbA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="<?php echo base_url('assets/js/scripts/administrador/reportes/contenido.js'); ?>"></script>
@endsection