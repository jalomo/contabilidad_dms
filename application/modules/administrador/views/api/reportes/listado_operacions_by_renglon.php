<div class="row">
    <div class="col-12">
        <div class="text-right">
            <button type="button" data-renglon_id="<?php echo $renglon_id; ?>" onclick="app.agregar_fomula(this)" class="btn btn-primary btn-sm mb-4"><i class="fas fa-plus"></i> Añadir formula</button>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col" style="width: 90px;">Operación</th>
                        <th scope="col">Origen de la información</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  $band=false; if(is_array($listado) && count($listado)>0){ ?>
                        <?php foreach ($listado as $key => $value) { ?>
                            <tr>
                                <th scope="row"><?php echo $value['orden']; ?></th>
                                <th scope="row"><center><?php echo ($band==true)? $value['tipo_operacion'] : ''; ?></center></th>
                                <td><?php echo ($value['tipo_calculo'] == 'CALCULO_SOBRE_CUENTAS')? $value['nombre_cuenta'] : $value['reporte_reglones'].'<br/><small><b>'.$value['reporte'].'</b></small>'; ?></td>
                                
                                <td><?php 
                                echo '<button title="Eliminar renglón" data-operacion_id= "'. $value['id'].'" data-renglon_id= "'. $value['reporte_reglon_id'].'" data-operacion_orden= "'. $value['orden'].'" data-tipo="eliminar" onclick="app.eliminar_operacion(this)" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                                ?></td>
                            </tr>        
                            <?php $band = true; ?>
                        <?php } ?>
                    <?php }else{ ?>
                            <tr>
                                <td colspan="4"><center>No hay registros disponibles</center></td>
                            </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>