<div class="row">
    <div class="col-sm-12">
        <form id="form_operaciones">

            <input type="hidden" value="<?php echo $renglon_id; ?>" name="renglon_id_actual" />

            <div class="form-group">
                <label><b> Origen de la información</b></label>
                <select name="origen_informacion" id="origen_informacion" onchange="app.cambiar_origen_informacion(this)" class="custom-select">
                    <option value=""></option>
                    <option value="1">Balanza</option>
                    <!-- <option value="2">Reporte actual</option> -->
                    <option value="3">Reportes</option>
                </select>
                <small id="msg_tipo_formato" class="form-text text-danger"></small>
            </div>
            
            <div class="form-group" id="div_cuenta_id" style="display:none">
                <label><b>* Cuenta</b></label>
                <select style="width:100%" name="cuenta_id" id="cuenta_id" class="custom-select">
                    <option value=""></option>
                    <?php foreach ($cuentas_dms as $cuenta) { ?>
                        <option value="<?php echo $cuenta['id']; ?>"><?php echo $cuenta['cuenta'] . '-' . $cuenta['decripcion']; ?></option>
                    <?php } ?>
                </select>
                <small id="msg_cuenta_id" class="form-text text-danger"></small>
            </div>

            <div class="form-group" id="div_reporte_id" onchange=" app.listado_reglones_reportes();" style="display:none">
                <label><b>* Reporte</b></label>
                <select style="width:100%" name="reporte_id" id="reporte_id" class="custom-select">
                    <option value=""></option>
                    <?php foreach ($reportes as $reporte) { ?>
                        <option value="<?php echo $reporte['id']; ?>"><?php echo $reporte['nombre']; ?></option>
                    <?php } ?>
                </select>
                <small id="msg_reporte_id" class="form-text text-danger"></small>
            </div>

            <div class="form-group" id="div_renglon_id" style="display:none">
                <label><b>* Renglon</b></label>
                <select style="width:100%" name="renglon_id" id="renglon_id" class="custom-select">
                </select>
                <small id="msg_renglon_id" class="form-text text-danger"></small>
            </div>

            <div class="form-group">
                <label><b>* Tipo operación</b></label>
                <select name="tipo_operacion" id="tipo_operacion" class="form-control">
                    <option value=""></option>
                    <option value="+">+ (SUMA)</option>
                    <option value="-">- (RESTA)</option>
                    <option value="/">/ (DIVICIÓN)</option>
                    <option value="*">* (MULTIPLICACIÓN)</option>
                </select>
                <small id="msg_tipo_operacion" class="form-text text-danger"></small>
            </div>
        </form>            

    </div>
</div>