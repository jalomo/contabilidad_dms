@layout('layout')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    FORMATO DE FLUJO DE BANCOS
</h4>

<div class="row">
    <div class="col-sm-12">
    <div class="card">
  <div class="card-body">
        <?php echo isset($crud['output'])? $crud['output'] : ''; ?> 
    </div>
    </div>
    </div>
</div>


@endsection

@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
<?php foreach($crud['css_files'] as $file){ ?>
    
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    
<?php } ?>

<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
    
    tfoot{
        display:none
    }
</style>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<?php foreach($crud['js_files'] as $file){ ?>
    
        <script src="<?php echo $file; ?>"></script>
    
<?php } ?>

<script>
$(function(){
    if($('.add_button').length){
        $('.add_button').addClass('btn').addClass('btn-primary');
    }

    if($('div.form-button-box input.ui-input-button').length){
        $('div.form-button-box input.ui-input-button').addClass('btn').addClass('btn-secondary');
    }

    if($('input.form-control').length){
        $('input.form-control').css('height','36px');
    }

    if($('input.form-control#field-fecha').length){
        $('input.form-control#field-fecha').attr('type','date');
    }
    if($('input.form-control#field-saldo').length){
        $('input.form-control#field-saldo').attr('step','0.01');
    }
    if($('input.form-control#field-haber').length){
        $('input.form-control#field-haber').attr('step','0.01');
    }
    if($('td.actions a').length){

        $( "td.actions" ).each(function( index ) {
            $(this).find('a:eq(0)').addClass('btn').addClass('btn-primary');
            $(this).find('a:eq(1)').addClass('btn').addClass('btn-danger');
        });
        
    }
    
})
</script>
@endsection