@layout('layout')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .bold {
        font-weight: bold;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
</style>
@endsection

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    Módulo de Cobranza
</h4>

<div class="card mt-3">
    <div class="card-body">
        <div class="row ">
            <div class="col-12">
                <h4 style="color:#323232; margin-bottom:50px">Cuentas morosas</h4>

                <table class="table table-striped" id="cobranza_dms" width="100%" cellspacing="0">
                    <thead>
                        <tr>

                            <th style="width:60%; border-right:1px solid #eee; text-align:center" class="" colspan="5">Datos generales</th>
                            <th style="text-align:center" class="" colspan="5">Abonos</th>
                        </tr>
                        <tr>
                            <th>Número cliente</th>
                            <th>Razón social</th>
                            <th>Nombre cliente</th>
                            <th>Concepto</th>
                            <th>Plazo credito</th>
                            <th>Saldo</th>
                            <th>1 a 30 días</th>
                            <th>31 a 60 días</th>
                            <th>61 a 90 días</th>
                            <th>Más de 90 días</th>
                            <th>-</th>
                        </tr>
                    </thead>
                </table>

            </div>
            <div class="col-md-12">
                <table class="table table-bordered col-md-5 mt-4" align="right">
                    <tr class="caption_table">
                        <th colspan="2" style="color:#fff">Totales</th>
                    </tr>
                    <tr>
                        <th class="text-right title_table" style="width:40%">Total:</th>
                        <td id="table_totaltotales" class="money_format">-</td>
                    </tr>
                    <tr>
                        <th class="text-right title_table">Total 1 - 30 días</th>
                        <td id="table_total130dias" class="money_format">-</td>
                    </tr>
                    <tr>
                        <th class="text-right title_table">Total 31 - 60 días:</th>
                        <td id="table_total3160dias" class="money_format">-</td>
                    </tr>
                    <tr>
                        <th class="text-right title_table">Total 61 - 90 días:</th>
                        <td id="table_total6190dias" class="money_format">-</td>
                    </tr>
                    <tr>
                        <th class="text-right title_table">Total 90 días ó más:</th>
                        <td id="table_total90masdias" class="money_format">-</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>
<div class="modal" id="modal_detalle" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalle cuenta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <tr>
                        <td style="width:30%" class="bold">Nombre: </td>
                        <td class="_nombre"></td>
                    </tr>
                    <tr>
                        <td class="bold">Razón social: </td>
                        <td class="_razonsocial"></td>
                    </tr>
                    <tr>
                        <td class="bold">Teléfono: </td>
                        <td class="_telefono"></td>
                    </tr>
                    <tr>
                        <td class="bold">Correo electrónico: </td>
                        <td class="_email"></td>
                    </tr>
                    <tr>
                        <td class="bold">Concepto: </td>
                        <td class="_concepto"></td>
                    </tr>
                    <tr>
                        <td class="bold">Total adeudo: </td>
                        <td class="_total_adeudo"></td>
                    </tr>
                </table>
                <table id="abonos_cuenta" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Fecha vencimiento</th>
                            <th>Dias vencidos</th>
                            <th>Total abono</th>
                            <th>Monto moratorio</th>
                            <th>Total pagar</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.2/jquery.mask.min.js" referrerpolicy="no-referrer"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    var app;
    let total_130dias = 0;
    let total_3160dias = 0;
    let total_6190dias = 0;
    let total_90masdias = 0;
    let suma_total = 0;
    var table = 'table#cobranza_dms';

    class Funciones {
        create_table() {
            $(table).DataTable(this.settings_table());
        }

        settings_table() {
            return {
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
                },
                columns: [{
                        title: 'Número cliente',
                        data: 'numero_cliente'
                    },
                    {
                        title: 'Razón social',
                        data: 'nombre_empresa'
                    },
                    {
                        title: 'Nombre cliente',
                        data: function(data) {
                            return data.nombre_cliente + ' ' + data.ap1_cliente + ' ' + data.ap2_cliente
                        }
                    },
                    {
                        title: 'Concepto',
                        data: 'concepto'
                    },
                    {
                        title: 'Plazo credito',
                        data: 'plazo_credito',
                    },
                    {
                        title: 'Total',
                        data: 'total',
                        render: function(data, type, row) {
                            return parseFloat(row.total_adeudo_moratorio).toFixed(2);
                        }
                    },
                    {
                        title: '1 a 30 días',
                        render: function(data, type, row) {
                            return parseFloat(row.total_abono130dias).toFixed(2);

                        }
                    },
                    {
                        title: '31 a 60 días',
                        render: function(data, type, row) {
                            return parseFloat(row.total_abono3160dias).toFixed(2);
                        }
                    },
                    {
                        title: '61 a 90 días',
                        render: function(data, type, row) {
                            return parseFloat(row.total_abono6190dias).toFixed(2);
                        }
                    },
                    {
                        title: 'Más de 90 días',
                        render: function(data, type, row) {
                            return parseFloat(row.total_abono90masdias).toFixed(2);
                        }
                    },
                    {
                        title: '-',
                        render: function(data, type, row) {
                            return '<button type="button" class="btn btn-dark" data-cuenta_id="' + row.id + '" onclick="app.openModal(this)"><i class="fa fa-file"></i></button>';
                        }
                    },

                ]
            }
        }

        get_busqueda() {
            $.ajax({
                dataType: "json",
                type: 'GET',
                dataSrc: "",
                url: API_URL_DMS + 'cuentas-morosas/buscar-filtro/',
                success: function(response) {
                    let listado = $(table).DataTable();
                    listado.clear().draw();
                    if (response && response.length > 0) {
                        response.forEach(listado.row.add);
                        listado.draw();
                    }
                }
            });
        }

        openModal(_this) {
            let cuenta_id = $(_this).data('cuenta_id');
            $("#modal_detalle").modal("show");
            $(".modal-body").hide();
            setTimeout(() => {
                $.ajax({
                    dataType: "json",
                    type: 'GET',
                    dataSrc: "",
                    url: API_URL_DMS + 'cuentas-morosas/buscar-filtro?cuenta_por_cobrar_id=' + cuenta_id,
                    success: function(response) {
                        $("#abonos_cuenta>tbody").html('');
                        let datos = response.shift();
                        $("._nombre").html(datos.nombre_cliente + ' ' + datos.ap1_cliente + ' ' + datos.ap2_cliente);
                        $("._razonsocial").html(datos.nombre_empresa);
                        $("._telefono").html(datos.telefono);
                        $("._email").html(datos.correo_electronico);
                        $("._concepto").html(datos.concepto);
                        $("._total_adeudo").html('$ <span class="money_format">' + parseFloat(datos.total_adeudo_moratorio).toFixed(2) + '</span>');
                        $(datos.abonos).each(function(index, val) {
                            $("#abonos_cuenta>tbody").append("<tr><td>" + val.fecha_vencimiento + "</td><td>" + val.dias_moratorios +"</td><td class='money_format'>" + parseFloat(val.total_abono).toFixed(2) +"</td><td class='money_format text-red'>" + parseFloat(val.monto_moratorio).toFixed(2) +"</td><td class='money_format'>" + parseFloat(val.total_moratorio).toFixed(2) +"</td></tr>");
                        });
                        $('.money_format').mask("#,##0.00", {
                            reverse: true
                        });
                        $(".modal-body").show();

                    }
                });
            }, 1000);

        }


    }

    $(function() {
        app = new Funciones();
        app.create_table();
        app.get_busqueda();
        setTimeout(function() {
            $('table#cobranza_dms tbody tr').each(function() {
                suma_total += parseFloat($(this).find("td").eq(4).text());
                total_130dias += parseFloat($(this).find("td").eq(5).text());
                total_3160dias += parseFloat($(this).find("td").eq(6).text());
                total_6190dias += parseFloat($(this).find("td").eq(7).text());
                total_90masdias += parseFloat($(this).find("td").eq(8).text());
                $(this).find("td").eq(4).addClass('money_format');
                $(this).find("td").eq(5).addClass('money_format');
                $(this).find("td").eq(6).addClass('money_format');
                $(this).find("td").eq(7).addClass('money_format');
                $(this).find("td").eq(8).addClass('money_format');
            });
            $("#table_totaltotales").html('$' + suma_total);
            $("#table_total130dias").html('$' + total_130dias);
            $("#table_total3160dias").html('$' + total_3160dias);
            $("#table_total6190dias").html('$' + total_6190dias);
            $("#table_total90masdias").html('$' + total_90masdias);
            $('.money_format').mask("#,##0.00", {
                reverse: true
            });
        }, 1000);
    });
</script>
@endsection