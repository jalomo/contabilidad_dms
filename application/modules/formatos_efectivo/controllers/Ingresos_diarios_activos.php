<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ingresos_diarios_activos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
        $crud = new grocery_CRUD();
        //$crud->set_theme('tablestrap');
        // $crud->setTable('tblItems');
        // $crud->setSubject('Item', 'Items');

        $crud->set_table('de_ingresos_diarios_activos');
        $crud->set_theme('datatables');

        if($this->uri->segment(4) == 'add' || $this->uri->segment(4) == 'edit'){
            $crud->field_type('fecha', 'string');
            $crud->field_type('fecha_de_validacion', 'string');
            $crud->field_type('monto', 'integer');
        }   
        //$crud->required_fields('id_origen_transaccion', 'monto','fecha');
        $crud->fields('fecha', 'concepto','fecha_de_validacion','vendedor','monto');
        $crud->columns(['fecha', 'concepto','fecha_de_validacion','vendedor','monto']);
        // $crud->display_as("id_origen_transaccion", "Origen transacción");

        // $crud->set_relation('id_origen_transaccion', 'ca_origen_transaccion', 'nombre');

        $crud->callback_delete(array($this,'delete_row'));
        $crud->where('de_ingresos_diarios_activos.deleted_at is null', null,false);
       

        $crud->unset_jquery();
        $crud->unset_jquery_ui();
        $crud->unset_bootstrap();
        $crud->unset_print();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_clone();
        
        
		$output = $crud->render();
        $this->render($output);
	}

    public function delete_row($primary_key)
    {
        return $this->db->update('de_ingresos_diarios_activos',array('deleted_at' => utils::now() ),array('id' => $primary_key));
    }

    public function render($output){ 
        // $this->load->helper(array('form', 'html', 'validation', 'url'));
        // utils::pre($output);
        $output = array(
            'crud' => (array)$output
        );
        $this->blade->render('/ingresos_diarios/ingresos_diarios_activos',$output);
    }
}
