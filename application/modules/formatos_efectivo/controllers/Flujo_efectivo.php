<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Flujo_efectivo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){ 
        $data = [
            'listado' => ''
        ];
        $this->blade->render('/flujo_efectivo/index',$data);
    }
    
}
