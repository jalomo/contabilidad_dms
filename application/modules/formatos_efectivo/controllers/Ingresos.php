<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ingresos extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){ 
        $data = [
            'listado' => ''
        ];
        $this->blade->render('/ingresos/index',$data);
    }
}
