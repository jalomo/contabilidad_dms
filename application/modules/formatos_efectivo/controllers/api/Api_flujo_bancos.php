<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_flujo_bancos extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ingresos_diarios(){
        
        $response = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $fecha = $this->input->post('fecha');

            $this->load->model('Asientos_model');
            $ingresos_egresos = $this->Asientos_model->get_flujo_bancos(array(
                'fecha_creacion' => $fecha
            ));

            $ingresos_egresos_total = $this->Asientos_model->get_total_bancos(array(
                //'fecha_creacion' => $fecha
            ),$fecha);
            

            

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                'FECHA','RENCIA','NOMBRE','CONCEPTO','VERIFICADO','DEBE','HABER','SALDO'
            ));
            
            
            $monto_abono_total = is_array($ingresos_egresos_total) && array_key_exists('abono',$ingresos_egresos_total)? $ingresos_egresos_total['abono'] : 0;
            $monto_cargo_total = is_array($ingresos_egresos_total) && array_key_exists('cargo',$ingresos_egresos_total)? $ingresos_egresos_total['cargo'] : 0;
            $monto_total = $monto_abono_total - $monto_cargo_total;

            $monto_cargo_total = 0;
            $monto_abono_total = 0;

            $cell_1 = array('data' => 'SALDO INICIAL', 'colspan' => 7,'class'=>'table-info');
            $cell_2 = array('data' => utils::formatMoney($monto_total,2), 'class'=>'table-info','style'=>'text-align:right');
            $this->table->add_row($cell_1, $cell_2);

            if(is_array($ingresos_egresos) && count($ingresos_egresos)>0){
                foreach ($ingresos_egresos as $value_ingreso_egresos) {
                    $monto_total = $monto_total + ($value_ingreso_egresos['abono'] - $value_ingreso_egresos['cargo']);
                    $monto_cargo_total = $monto_cargo_total + $value_ingreso_egresos['cargo'];
                    $monto_abono_total = $monto_abono_total + $value_ingreso_egresos['abono'];

                    $cell_abono = array('data' => utils::formatMoney($value_ingreso_egresos['abono'],2), 'style'=>'text-align:right');
                    $cell_cargo = array('data' => utils::formatMoney($value_ingreso_egresos['cargo'],2), 'style'=>'text-align:right');
                    $cell_total = array('data' => utils::formatMoney($monto_total,2), 'style'=>'text-align:right');

                    $this->table->add_row([
                        utils::aFecha($value_ingreso_egresos['fecha_creacion'],true),
                        $value_ingreso_egresos['referencia'],
                        $value_ingreso_egresos['nombre'],
                        $value_ingreso_egresos['concepto'],

                        utils::aFecha($value_ingreso_egresos['created_at'],true),
                        $cell_cargo,
                        $cell_abono,
                        $cell_total
                        
                    ]);
                    
                }
            }else{
                $cell = array('data' => '<center>No se encontraron movimientos</center>', 'colspan' => 8);
                $this->table->add_row($cell);
            }

            $cell_1 = array('data' => 'TOTAL', 'colspan' => 5,'class'=>'table-info');
            $cell_2 = array('data' => utils::formatMoney($monto_abono_total,2), 'class'=>'table-info', 'style'=>'text-align:right');
            $cell_3 = array('data' => utils::formatMoney($monto_cargo_total,2), 'class'=>'table-info', 'style'=>'text-align:right');
            $cell_4 = array('data' => utils::formatMoney($monto_total,2), 'class'=>'table-info', 'style'=>'text-align:right');
            $this->table->add_row($cell_1, $cell_3, $cell_2, $cell_4);

            
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }
    

    public function egresos_diarios(){
        $response = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $fecha = $this->input->post('fecha');

            $this->load->model('Asientos_model');
            $egresos = $this->Asientos_model->get_listado_egresos(array(
                'fecha_creacion' => $fecha
            ));

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                'FECHA','CONCEPTO','FECHA DE VALIDACION','MONTO'
            ));
            
            $monto_total = 0;
            if(is_array($egresos) && count($egresos)>0){
                foreach ($egresos as $value_egreso) {
                    $this->table->add_row([
                        utils::aFecha($value_egreso['fecha_creacion'],true),
                        $value_egreso['concepto'],
                        utils::aFecha($value_egreso['created_at'],true),
                        utils::formatMoney($value_egreso['cargo'],2)
                    ]);
                    $monto_total = $monto_total + $value_egreso['cargo'];
                }
            }else{
                $cell = array('data' => '<center>No se encontraron movimientos</center>', 'colspan' => 4);
                $this->table->add_row($cell);
            }

            $cell = array('data' => 'TOTAL', 'colspan' => 3);
            $this->table->add_row($cell, utils::formatMoney($monto_total,2));

            
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }
}
