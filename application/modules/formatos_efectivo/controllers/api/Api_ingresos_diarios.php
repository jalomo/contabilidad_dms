<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_ingresos_diarios extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ingresos_diarios(){
        $response = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $fecha = $this->input->post('fecha');

            $this->load->model('DeIngresosDiariosActivos_model');
            $ingresos = $this->DeIngresosDiariosActivos_model->getAll(array(
                'fecha' => $fecha
            ));
            //$egresos = $this->Asientos_model->get_listado_egresos();

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                'FECHA','CONCEPTO','FECHA DE VALIDACION','VENDEDOR','MONTO'
            ));
            
            $monto_total = 0;
            if(is_array($ingresos) && count($ingresos)>0){
                foreach ($ingresos as $key_ingreso => $value_ingreso) {
                    $this->table->add_row([
                        utils::aFecha($value_ingreso['fecha'],true),
                        $value_ingreso['concepto'],
                        utils::aFecha($value_ingreso['fecha_de_validacion'],true),
                        $value_ingreso['vendedor'],
                        utils::formatMoney($value_ingreso['monto'],2)
                    ]);
                    $monto_total = $monto_total + $value_ingreso['monto'];
                }
            }else{
                $cell = array('data' => '<center>No se encontraron movimientos</center>', 'colspan' => 5);
                $this->table->add_row($cell);
            }

            $cell = array('data' => 'TOTAL', 'colspan' => 4);
            $this->table->add_row($cell, utils::formatMoney($monto_total,2));

            
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }
    

    public function egresos_diarios(){
        $response = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fecha', 'Fecha', 'trim|required');
        
        if ($this->form_validation->run($this) != FALSE)
        {

            $fecha = $this->input->post('fecha');

            $this->load->model('DeIngresosDiariosPasivos_model');
            $egresos = $this->DeIngresosDiariosPasivos_model->getAll(array(
                'fecha' => $fecha
            ));

            $this->load->library('table');
            $template = array(
                'table_open' => '<table class="table table-bordered table-striped" >'
            );

            $this->table->set_template($template);

            $this->table->set_heading(array(
                'FECHA','CONCEPTO','FECHA DE VALIDACION','MONTO'
            ));
            
            $monto_total = 0;
            if(is_array($egresos) && count($egresos)>0){
                foreach ($egresos as $value_egreso) {
                    $this->table->add_row([
                        utils::aFecha($value_egreso['fecha'],true),
                        $value_egreso['concepto'],
                        utils::aFecha($value_egreso['fecha_de_validacion'],true),
                        utils::formatMoney($value_egreso['monto'],2)
                    ]);
                    $monto_total = $monto_total + $value_egreso['monto'];
                }
            }else{
                $cell = array('data' => '<center>No se encontraron movimientos</center>', 'colspan' => 4);
                $this->table->add_row($cell);
            }

            $cell = array('data' => 'TOTAL', 'colspan' => 3);
            $this->table->add_row($cell, utils::formatMoney($monto_total,2));

            
            $this->contenido->data = array(
                'tabla' => base64_encode($this->table->generate())
            ); 
        }
        else
        {
            $this->contenido->error = true;
            $this->contenido->mensaje = $this->form_validation->error_array();
        }

        $this->response();
    }
}
