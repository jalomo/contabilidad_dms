<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Flujo_bancos_crud extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
        $crud = new grocery_CRUD();
        //$crud->set_theme('tablestrap');
        // $crud->setTable('tblItems');
        // $crud->setSubject('Item', 'Items');

        $crud->set_table('de_formato_flujo_efectivo');
        $crud->set_theme('datatables');
        if($this->uri->segment(4) == 'add' || $this->uri->segment(4) == 'edit'){
            $crud->field_type('fecha', 'string');
            $crud->field_type('saldo', 'integer');
        }
        

        $crud->unset_jquery();
        $crud->unset_jquery_ui();
        $crud->unset_bootstrap();
        $crud->unset_print();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_clone();
        
        //$crud->columns(['id','fecha', 'rencia', 'nombre', 'concepto', 'verificado', 'debe', 'haber', 'saldo']);
        
		$output = $crud->render();
        $this->render($output);
	}

    public function render($output){ 
        // $this->load->helper(array('form', 'html', 'validation', 'url'));
        // utils::pre($output);
        $output = array(
            'crud' => (array)$output
        );
        $this->blade->render('/flujo_bancos_crud/render',$output);
    }
}
