@layout('layout')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    INGRESOS DIARIOS
</h4>

<div class="row">
    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-light bg-light">
                            <form class="form-inline" id="form_busqueda">
                                <span class="navbar-text mr-2">
                                    <b>Fecha: </b>
                                </span>
                                <input class="form-control mr-sm-2" type="date" name="fecha" placeholder=""
                                    aria-label="" value="<?php echo utils::get_date(); ?>" max="<?php echo utils::get_date(); ?>">
                                    <small id="msg_fecha" class="form-text text-danger"></small>
                                <button class="btn btn-primary ml-2 my-2 my-sm-0" onclick="buscar();" type="button">Buscar</button>
                            </form>
                        </nav>
                    </div>
                </div>
                <div class="row mt-5">
                     <div class="col-12">
                            <div class="text-right">
                                <a type="button" href="<?php echo site_url('formatos_efectivo/ingresos_diarios_activos'); ?>"  class="btn btn-primary btn-sm mb-4"> Modificar contenido</a>
                            </div>
                        </div>
                    <div class="col-sm-12">
                        <div class="table-responsive" id="primer_reporte">
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    
                        <div class="col-12">
                            <div class="text-right">
                                <a type="button" href="<?php echo site_url('formatos_efectivo/ingresos_diarios_pasivos'); ?>"  class="btn btn-primary btn-sm mb-4"> Modificar contenido</a>
                            </div>
                        </div>
                    
                    <div class="col-sm-12">
                        <div class="table-responsive" id="segundo_reporte"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('style')
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
    
</style>
@endsection

@section('script')
<script>
    function buscar(){
        $('small.form-text.text-danger').html('');
        Pace.track(function() {
            $.ajax({
                type: 'POST',
                url: PATH+'/formatos_efectivo/api/api_ingresos_diarios/ingresos_diarios',
                data: $('form#form_busqueda').serializeArray(),
                dataType: "json",
                traditional: true,
                beforeSend: function(){
                    $('div#primer_reporte').html('<div class="fa-5x"><center><i class="fas fa-cog fa-spin"></i></center></div>');
                },
                success: function (data) {
                    if(data.error == true){
                        $.each(data.mensaje, function( index, value ) {
                            if( $('small#msg_'+index).length ){
                                $('small#msg_'+index).html(value);
                            }
                        });
                    } else {
                        let listado = atob(data.data.tabla);
                        $('div#primer_reporte').html(listado);
                    } 
                                
                }
            });
        });

        Pace.track(function() {
            $.ajax({
                type: 'POST',
                url: PATH+'/formatos_efectivo/api/api_ingresos_diarios/egresos_diarios',
                data: $('form#form_busqueda').serializeArray(),
                dataType: "json",
                traditional: true,
                beforeSend: function(){
                    $('div#segundo_reporte').html('<div class="fa-5x"><center><i class="fas fa-cog fa-spin"></i></center></div>');
                },
                success: function (data) {
                    if(data.error == true){
                        $.each(data.mensaje, function( index, value ) {
                            if( $('small#msg_'+index).length ){
                                $('small#msg_'+index).html(value);
                            }
                        });
                    } else {
                        let listado = atob(data.data.tabla);
                        $('div#segundo_reporte').html(listado);
                    } 
                                
                }
            });
        });
    }

    $(function(){
        buscar();
    })
</script>
@endsection