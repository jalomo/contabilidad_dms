@layout('layout')

@section('contenido')

<style>
    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
    }
</style>
<div class="row">
    <div class="card mt-5" style="width: 100%;">
        <div class="card-body">

            <h4 class="mt-2 mb-2 text-gray-800 text-center">
                INGRESOS DIARIOS<br />
                22 DE SEPTIEMBRE DE 2021
            </h4>

            <div class="col-sm-12">

                <table class="table table-striped">

                    <tbody>

                        <tr class="table-primary">
                            <td height="33" class="xl239" style="height:24.75pt"></td>
                            <td colspan="3" class="xl347"><b>SALDO INICIAL</b></td>


                            <td class="xl242"></td>
                            <td class="xl243" align="right">3,572,126.38 </td>

                        </tr>


                        <tr height="28" style="height:21.0pt">

                            <td class="xl280">FECHA</td>
                            <td class="xl65"></td>
                            <td class="xl65">CONCEPTO</td>
                            <td class="xl65">FECHA DE VALIDACION</td>
                            <td class="xl281">VENDEDOR</td>
                            <td class="xl249"></td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">ANDRES GARCIA PAGO CUENTA DE TERCERO/ 0083886016</td>
                            <td class="xl254">UNIDAD 02-08-21</td>
                            <td class="xl281">NAOMI</td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>20,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS DEBITO/146083455</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>2,548.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS CREDITO/146083455</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>26,544.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS PUNTOS TDC BANCOME/146083455</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>2,125.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS DEBITO/144115630</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>1,255.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS CREDITO/144115630</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>10,500.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS CREDITO/146083455</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>8,500.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS DEBITO/144115630</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>7,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">VENTAS CREDITO/144115630</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>25,111.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">SPEI RECIBIDOSANTANDER/0191542472 014</td>
                            <td class="xl254">UNIDAD 02-08-21</td>
                            <td class="xl281">JUAN CARLOS</td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>20,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl252"></td>
                            <td class="xl253">DEPOSITO EFECTIVO PRACTIC/******1260</td>
                            <td class="xl254"></td>
                            <td class="xl281"></td>
                            <td class="xl255"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>5,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">PAGO CUENTA DE TERCERO/ 005918</td>
                            <td class="xl258"></td>
                            <td class="xl282"></td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>3,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">PAGO CUENTA DE TERCERO/ 0075012</td>
                            <td class="xl258"></td>
                            <td class="xl282"></td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>10,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">FRANCISCO JAVIER ARRILLAGA</td>
                            <td class="xl258"></td>
                            <td class="xl282"></td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>70,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">DEP TRAS TARJETA<span style="mso-spacerun:yes">
                                </span>392232</td>
                            <td class="xl258"></td>
                            <td class="xl282"></td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>15,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">ABEL CASTRO B</td>
                            <td class="xl254">SEMINUEVO 02-08-21</td>
                            <td class="xl282">ISAIAS</td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$ 500,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">IVAN CASTILLO S</td>
                            <td class="xl254">SEMINUEVO 02-08-21</td>
                            <td class="xl282">EDAR</td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$ 200,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">TARJETA</td>
                            <td class="xl258"></td>
                            <td class="xl282"></td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>15,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">TARJETA</td>
                            <td class="xl258"></td>
                            <td class="xl282"></td>
                            <td class="xl260"><span style="mso-spacerun:yes"></span>$ 500,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">SPEI RECIBIDOSANTANDER/01920182 014</td>
                            <td class="xl261"></td>
                            <td class="xl282"></td>
                            <td class="xl249"><span style="mso-spacerun:yes"></span>$<span </tr> <tr height="28"
                                    style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">DEPOSITO DE TERCERO/REFBNTC004941</td>
                            <td class="xl261"></td>
                            <td class="xl282"></td>
                            <td class="xl249"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>4,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">TARJETA</td>
                            <td class="xl261"></td>
                            <td class="xl282"></td>
                            <td class="xl249"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>5,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">TARJETA</td>
                            <td class="xl254">UNIDAD 02-08-21</td>
                            <td class="xl282">ADRIAN</td>
                            <td class="xl249"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>60,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl65"></td>
                            <td class="xl257">APARTADO<span style="mso-spacerun:yes"> </span>0121212 SPEI</td>
                            <td class="xl261"></td>
                            <td class="xl282"></td>
                            <td class="xl249"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes"> </span>15,000.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right"></td>
                            <td class="xl65"></td>
                            <td class="xl257">DEPOSITO EN EFECTIVO</td>
                            <td class="xl261"></td>
                            <td class="xl259"></td>
                            <td class="xl249"><span style="mso-spacerun:yes"></span>$ 242,117.00 </td>

                        </tr>
                        <tr height="28" style="height:21.0pt">

                            <td class="xl251" align="right"></td>
                            <td class="xl251" align="right"></td>
                            <td class="xl251" align="right"></td>
                            <td class="xl263"></td>
                            <td class="xl65"></td>
                            <td class="xl264"><span style="mso-spacerun:yes"></span><b>$ 1,770,000.00</b></td>
                        </tr>

                        <tr height="27" style="height:20.25pt">
                            <td colspan="6" class="table-primary mt-3">EGRESOS DEL DIA<span
                                    style="mso-spacerun:yes"></span></td>
                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">FORD CREDIT DE MEXICO SA DE CV</td>
                            <td class="xl65">NETEO</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>600,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">MYLSA QUERETARO SA DE CV</td>
                            <td class="xl65">ENJHANCHE FIGO</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>5,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129" align="right">36741</td>
                            <td class="xl129">ROSA ELVA LARA HERNANDEZ</td>
                            <td class="xl65">REEMBOLSO</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>5,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">KARINA GUADALIUPEM PEÑA BRIONES</td>
                            <td class="xl65">PIPAS DE AGUA</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>8,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">GRUPO ECOLOGICO DE LIMPIEZA INDUSTRIAL</td>
                            <td class="xl65">SERVD E LIMPIEZA</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>50,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">JUDITH PEREZ NIETO</td>
                            <td class="xl65">TONER</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>60,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">TELEFONOS D EMEXICO</td>
                            <td class="xl65">SERV DE INTERNET</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>5,606.70 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">CARMELO LAVIN FERNANDEZ</td>
                            <td class="xl65">ART DE LAVADO</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>11,480.09 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">MARIA DEL ROPSARIO LOPEZ</td>
                            <td class="xl65">MATERIALES</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>685,088.21 </td>
                            <td class="xl239"></td>
                            <td class="xl268"></td>
                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">FORD CREDIT DE MEXICO SA DE CV</td>
                            <td class="xl65">REFACCIONES</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>25,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">LLOURDES YADIRA HERNANDEZ</td>
                            <td class="xl65">DEVOLUCION</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>825.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">COMISIONES BANCARIAS</td>
                            <td class="xl65">COMISINES</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>12,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">FORD CREDIT DE MEXIC04929/FCM950703 D3A</td>
                            <td class="xl65">DOMICILIACION</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>10,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">FORD CREDIT DE MEXIC04929/FCM950703 D3A</td>
                            <td class="xl65">DOMICILIACION</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>10,000.00 </td>

                        </tr>
                        <tr height="20" style="height:15.0pt">

                            <td class="xl251" align="right">02/08/2021</td>
                            <td class="xl129"></td>
                            <td class="xl129">FORD CREDIT DE MEXIC04929/FCM950703 D3A</td>
                            <td class="xl65">DOMICILIACION</td>

                            <td class="xl268"></td>
                            <td class="xl268"><span style="mso-spacerun:yes"></span>$<span
                                    style="mso-spacerun:yes">
                                </span>12,000.00 </td>

                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: right;">SALDO INICIAL +  INGRESOS DEL DIA - EGRESOS DEL DIA  14-09-21</td>
                            <td><b>$ 3,842,126.38</b></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: right;">PERDIDA O GANANCIA DEL DIA</td>
                            <td><b>$ 270,000.00</b></td>
                        </tr>
                    <tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection