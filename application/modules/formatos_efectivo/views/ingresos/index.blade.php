@layout('layout')

@section('contenido')
<h4 class="mt-4 mb-4 text-gray-800 text-center">
    INGRESOS
</h4>

<div class="row">
    <div class="col-12">
        <div class="text-right">
            <a type="button" href="<?php echo site_url('formatos_efectivo/ingresos_crud'); ?>"  class="btn btn-primary btn-sm mb-4"> Modificar contenido</a>
        </div>
    </div>
</div>
<div class="row">


    <div class="col-sm-12">
        <div class="card bg-light mb-3 mb-4">
            <div class="card-body">

               

                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-light bg-light">
                            <form class="form-inline" id="form_busqueda">
                                <span class="navbar-text mr-2">
                                    <b>Fecha: </b>
                                </span>
                                <select value="<?php echo date('m'); ?>" name="mes" class="form-control mr-sm-2"></select>
                                <input class="form-control mr-sm-2" type="number" name="anio" placeholder=""
                                    aria-label="" value="<?php echo date('Y'); ?>" min="2020" max="<?php echo date('Y'); ?>">
                                    <small id="msg_fecha" class="form-text text-danger"></small>
                                <button class="btn btn-outline-success ml-2 my-2 my-sm-0" onclick="buscar();" type="button">Buscar</button>
                            </form>
                        </nav>
                    </div>
                </div>
                <div class="contenido_general  mt-5">
                </div>
            </div>
        </div>
    </div>
</div>

<script id="tmp_calculos_semana" type="text/template">
[{#contenido}]
<div class="row mt-4">
    <div class="col-sm-12">
        <div class="card" style="width: 100%;">
            <div class="card-body">
                <div id="fecha_semana_num_[{semana}]" class="col-sm-12">
                    <h5 class="card-title">Semana<br/>[{titulo}]</h5>
                    <div class="mt-5 contenido_tabla"><center><i class="fa fa-cog fa-spin fa-4x fa-fw"></i></center></div>
                </div>
            </div>
        </div>
    </div>
</div>
[{/contenido}]
</script>


@endsection

@section('style')
<style>
    #content {
        background: #f3f4f5 !important;
    }

    .card-body {
        background-color: white;
    }

    .title_table {
        font-weight: bold;
        background: #eee !important;
        color: #323232;
    }

    .caption_table {
        font-weight: bold;
        background: #4E73DF;
        color: #fff;
    }

    .table td,
    .table th {
        padding: .45rem !important;
        vertical-align: top;
        border-top: 1px solid #e3e6f0;
        font-size: 14px !important;
        color: #000;
    }
    
</style>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.2.1/mustache.min.js" integrity="sha512-Qjrukx28QnvFWISw9y4wCB0kTB/ISnWXPz5/RME5o8OlZqllWygc1AB64dOBlngeTeStmYmNTNcM6kfEjUdnnQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

    function zfill(number, width) {
        var numberOutput = Math.abs(number); /* Valor absoluto del número */
        var length = number.toString().length; /* Largo del número */ 
        var zero = "0"; /* String de cero */  
        
        if (width <= length) {
            if (number < 0) {
                return ("-" + numberOutput.toString()); 
            } else {
                return numberOutput.toString(); 
            }
        } else {
            if (number < 0) {
                return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
            } else {
                return ((zero.repeat(width - length)) + numberOutput.toString()); 
            }
        }
    }

    
    function obtener_numero_semana(mJsDate){
        if(mJsDate.isValid()){
            if($.inArray(mJsDate.isoWeekday(),[6,7]) == '-1' ){
                return  mJsDate.format('WW');
            }else{
                return null;
            }
                    
        }else{
            return null;
        }
    }

    function obtener_dias(dayStr,endOfMonth){
        var dias = [];
        dias.push(dayStr.format('Y-MM-DD'));
        for (let index = dayStr.isoWeekday(); index < 5; index++) {
            dayStr = dayStr.add(1, 'days');
            if(dayStr <= endOfMonth){
                dias.push(dayStr.format('Y-MM-DD'));
            }
        }
        return dias;
    }

    function buscar(){

        $('small.form-text.text-danger').html('');
        $('div.contenido_general').html('');

        var anio = zfill($('input[name=anio]').val(),4);
        var mes = zfill($('select[name=mes] option:selected').val(), 2);

        var semanas = [];
        var semanas_dias = [];

        let endOfMonth   = moment(anio+"-"+zfill(mes,2)+'-'+ zfill(1,2)).add(1, 'months').subtract(1, 'months').endOf('month');
        
        for(var i = 1; i <= 31; i++) {
            var dayStr = moment(anio+"-"+zfill(mes,2)+'-'+ zfill(i,2));
            var calc_semana = obtener_numero_semana(dayStr);

            var semana_actual = moment();

            if((calc_semana != null) && (calc_semana <= semana_actual.format('WW'))){
                console.log(calc_semana);
                if($.inArray(calc_semana,semanas) == '-1' ){
                    semanas.push(calc_semana);
                    var dias_semana = obtener_dias(dayStr,endOfMonth);

                    semanas_dias = {
                        'semana': calc_semana,
                        'dias': JSON.stringify(dias_semana)
                    };

                    var fecha_str = '';
                    try {
                        fecha_str = moment(dias_semana[0]).format('LL') + ' al ' + moment(dias_semana[dias_semana.length - 1]).format('LL');
                    } catch (error) {
                        fecha_str = '';
                    }
                    var contenido = {
                        contenido: {
                            titulo: fecha_str,
                            semana: calc_semana
                        }
                    }; 
                        
                    var customTags = ['[{','}]'];
                    var template = document.getElementById('tmp_calculos_semana').innerHTML;
                    var rendered = Mustache.render(template, contenido ,{},customTags);
                    $('div.contenido_general').append(rendered);

                    get_contenido(calc_semana,semanas_dias);

                }
            }
        }
    }
    
    function get_contenido(calc_semana,semanas_dias){
        Pace.track(function() {
            $.ajax({
                type: 'POST',
                url: PATH+'/formatos_efectivo/api/api_ingresos/buscar',
                data: semanas_dias,
                dataType: "json",
                traditional: true,
                success: function (data) {
                    if(data.error == true){
                        $.each(data.mensaje, function( index, value ) {
                            if( $('small#msg_'+index).length ){
                                $('small#msg_'+index).html(value);
                            }
                        });
                    } else {
                        let listado = atob(data.data.tabla);
                        $('div#fecha_semana_num_'+calc_semana+' div.contenido_tabla center').remove();
                        $('div#fecha_semana_num_'+calc_semana+' div.contenido_tabla').html(listado);
                    } 
                                
                }
            });
        });
    }
    

    $(function(){

        Pace.stop();

        moment.locale('es');
        var value = $('select[name=mes]').attr('value');
        for (let index = 1; index <= 12; index++) {
            var mes = moment(index, "MM").format('MMMM')
            if(zfill(index,2) == value){
                $('select[name=mes]').append('<option value="'+zfill(index,2)+'" selected>'+mes+'</option>');
            }else{
                $('select[name=mes]').append('<option value="'+zfill(index,2)+'">'+mes+'</option>');
            }
        }
        buscar();

    })
</script>
@endsection