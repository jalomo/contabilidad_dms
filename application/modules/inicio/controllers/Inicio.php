<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Inicio extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
  }


  public function index()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST') {
      $this->form_validation->set_rules('password', 'Contraseña', 'required');
      $this->form_validation->set_rules(
        'usuario',
        'Usuario',
        'required|callback_username_check',
        array('username_check' => 'El nombre de usuario o la contraseña son incorrectos.')
      );

      if ($this->form_validation->run($this) != FALSE) {
        redirect('administrador');
      }
    }

    $this->blade->render('inicio/index');
  }

  public function username_check()
  {
    $this->load->model('CaReportes_model');
    if (strlen($this->input->post('usuario')) > 0) {

      $query = $this->db->from('ca_usuarios')->where(array(
        'usuario' => $this->input->post('usuario'),
        'contrasena' => ($this->input->post('password'))
      ))->get();
      $result = $query->num_rows();
      $result_data = $query->row_array();

      $reportes = $this->CaReportes_model->getAll();
      if ($result >  0) {
        $this->session->set_userdata($result_data);
        $this->session->set_userdata('reportes', $reportes);
        $this->session->set_userdata('session_tipo', 1);
        $this->session->set_userdata('logged_in', true);
        return TRUE;
      }

      return FALSE;
    }
  }
}
