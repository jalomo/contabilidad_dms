@layout('layout_login')

@section('contenido')
<div class="sep50"></div>
<center>
    <img width="300px" src="{{ base_url('assets/img/dmslogo.png') }}" class="img-fluid" />
    <h3 class="mt-2 bold">Contabilidad DMS SAN JUAN</h3>
</center>
<div class="col-md-6 col-centered mt-3 mb-5 panel-theme">
    <p class="bold">Iniciar sesión para poder acceder al sistema de Contabilidad</p>
    <form name="login" action="<?php echo site_url('inicio/index'); ?>" method="post">
        <div class="form-group">
            <label><b>Usuario</b></label>
            <input type="text" name="usuario" class="form-control" />
            <small id="msg_usuario" class="form-text text-danger"><?php echo form_error('usuario'); ?></small>
        </div>
        <div class="form-group">
            <label><b>Contraseña</b></label>
            <input type="password" name="password" class="form-control" />
            <small id="msg_password" class="form-text text-danger"><?php echo form_error('password'); ?></small>
        </div>
        <div class="float-right">
            <button type="submit" id="loginbtn" class="btn btn-primary"><i class="fa fa-play"></i> Entrar</button>
        </div>
        <div class="sep10"></div>
    </form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection