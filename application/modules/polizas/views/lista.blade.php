@layout('layout')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('contenido')
<h1 class="h3 mb-4 text-gray-800">Lista de poliza</h1>
<form action="<?php echo base_url(); ?>index.php/polizas/lista_polizas" method="get" id="consultar">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Año</label>
                <select class="form-control" name="anio">
                    <option value="2021">2021</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Mes</label>
                <select class="form-control" name="mes">
                    <option value="1">Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Todo el mes?</label>
                <select class="form-control" name="dia">
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Día</label>
                <select class="form-control" name="dia">
                    <?php for ($i = 1; $i < 32; $i++) : ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        <div class="col-md-2 mt-1">
            <button type="submit" class="btn btn-primary mt-4">Consultar</button>
        </div>
    </div>
</form>
<hr />
<a class="btn btn-primary mt-2 mb-4" href="<?php echo site_url('polizas/index');?>"><i class="fa fa-plus-circle"></i>&nbsp;Alta de Poliza</a>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Fecha</th>
            <th>Poliza</th>
            <th>Concepto</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Fecha</th>
            <th>Poliza</th>
            <th>Concepto</th>
            <th>Opciones</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($lista as $row) : ?>
            <tr>
                <th><?php echo $row->fecha; ?></th>
                <th><?php echo $row->poliza; ?></th>
                <th><?php echo $row->concepto; ?></th>
                <th>
                    <a href="<?php echo base_url(); ?>index.php/asientos/ver/<?php echo $row->id_id; ?>">
                        <button type="button" class="btn btn-primary">Ver</button>
                    </a>
                </th>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $('#guardar_formulario').submit(function(event) {
            event.preventDefault();
            var url = "<?php echo base_url() ?>index.php/polizas/guarda";
            ajaxJson(url, {
                    "fecha": $("#fecha").val(),
                    "poliza": $("#poliza").val(),
                    "concepto": $("#concepto").val()
                },
                "POST", "",
                function(result) {
                    correoValido = false;
                    console.log(result);
                    json_response = JSON.parse(result);
                    obj_output = json_response.output;
                    obj_status = obj_output.status;
                    if (obj_status == false) {
                        aux = "";
                        $.each(obj_output.errors, function(key, value) {
                            aux += value + "<br/>";
                        });
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                    }
                    if (obj_status == true) {
                        exito_redirect(" GUARDADO CON EXITO", "success", "<?php echo base_url() ?>index.php/polizas/index");
                    }
                });
        });
        $("#dataTable").DataTable({
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
            }
        });
    });
</script>
@endsection