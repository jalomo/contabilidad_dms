<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Polizas extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
       
    }

    public function index(){
        $datos['index'] = '';
        
        $this->blade->render('alta',$datos);
    }

    public function guarda(){
        $this->form_validation->set_rules('fecha', 'fecha', 'required');
        $this->form_validation->set_rules('poliza', 'poliza', 'required');
        $this->form_validation->set_rules('concepto', 'concepto', 'required');
  
        $response = validate($this);
  
  
        if($response['status']){
          $data['fecha'] = $this->input->post('fecha');
          $data['poliza'] = $this->input->post('poliza');
          $data['concepto'] = $this->input->post('concepto');
          $data['fecha_creacion'] = date('Y-m-d H:i:s');
          $data['id_id'] = get_guid();
          $data['folio'] = "0";
          $data['cargo'] = "0";
          $data['abono'] = "0";
  
          $this->Mgeneral->save_register('polizas', $data);
        }
      //  echo $response;
      echo json_encode(array('output' => $response));
    }

    public function lista_polizas(){
        if((isset($_GET['anio'])) & (isset($_GET['mes'])) & (isset($_GET['dia']))){
            $datos['lista'] = $this->Mgeneral->get_table('polizas');
        }else{
            $datos['lista'] = $this->Mgeneral->get_table('polizas');
        }
        $datos['index'] = '';
       
        $this->blade->render('lista',$datos);
    }

    
  
}