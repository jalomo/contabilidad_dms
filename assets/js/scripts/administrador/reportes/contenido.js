var ctrl_contenido;

class CTRL_PRINCIPAL {

    open_modal_reporte(_this) {
        let tipo = $(_this).data('tipo');
        $("input[name*='reporte_nombre']").val( $('input#form_nombre_reporte').val() );
        $("div#modal_reporte").modal("show");

    }

    guardar_nombre_reporte(_this) {
        if (!$("input[name*='reporte_nombre']").val().length > 1) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'El nombre del reporte es obligatorio',
            })
        }else{

            $.ajax({
                url: PATH + '/administrador/api/reportes/save_reporte',
                type: 'POST',
                data: {
                    reporte_id: $("input#reporte_id").val(),
                    reporte_nombre: $("input[name='reporte_nombre']").val()
                },
                success: function(response) {
                    if (response.estatus != "error") {

                        $("div#modal_reporte").modal("hide");
                        $('input#form_nombre_reporte').val( $("input[name*='reporte_nombre']").val() );

                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito!',
                            text: response.mensaje,
                        }).then((result) => {
                            app.get_busqueda();
                        });

                    } else {
                        if (response.errores) {
                            $.each(response.errores, function(index, value) {
                                if ($("small#msg_" + index).length) {
                                    $("small#msg_" + index).html(value);
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.mensaje,
                            })
                        }
                    }
                },
                error: function(respuesta) {
                    console.log(respuesta);
                }
            });

        }
    }

}
$(function(){
    ctrl_contenido = new CTRL_PRINCIPAL();

})









var app;
var table = 'table#contenido_reporte';
var table_operaciones = 'table#operaciones';
var cliente_id = '';
var reporte_renglon_id = '';
var reporte_id = $("#reporte_id").val()

var detailRows = [];
var dt_table;
 

class Funciones {
    create_table() {
        dt_table = $(table).DataTable(this.settings_table());
    }
    // create_table_operaciones() {
    //     $(table_operaciones).dataTable(this.settings_table_operaciones());

    // }

    settings_table() {
        return {
            ajax: {
                url: PATH + '/administrador/api/reportes/listado_contenido',
                type: "GET",
                data: function(){
                    return {reporte_id: reporte_id};
                }
            },
            lengthChange: false,
            order: [
                [0, "asc"]
            ],
            aLengthMenu: [100],
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
            },
            columns: [
                {
                    title: '#',
                    render: function(data, type, row) {
                        return parseInt(row.orden)
                    }
                },
                {
                    title: 'Cuenta',
                    data: 'nombre'
                },
                {
                    title: 'Tipo',
                    render: function(data, type, row) {
                        switch (parseInt(row.tipo)) {
                            case 1:
                                return 'Operación';
                                break;
                            case 2:
                                return 'Subtitulo';
                                break;
                            case 3:
                                return 'Título';
                                break;
                            default:
                                return '';
                                break;
                        }
                    }
                },
                {
                    title: 'Formato',
                    data: 'formato'
                },
                {
                    title: 'Acciones',
                    width: '140px',
                    render: function(data, type, row) {
                        let editar = '';
                        let operaciones = '';
                        let eliminar = '';
                        editar = '<button title="Editar cuenta" data-renglon_id= "' + row.id + '" data-nombre= "' + row.nombre + '" data-formato= "' + row.formato + '" data-tipo_renglon= "' + row.tipo + '" onclick="app.open_modal_cuenta(this)" class="btn btn-secondary btn-sm"><i class="fas fa-pencil-alt"></i></button>';
                        operaciones = '<button title="Operacion" data-renglon_id= "' + row.id + '" data-nombre= "' + row.nombre + '"  onclick="app.open_operaciones(this)" class="btn btn-info btn-sm"><i class="fas fa-calculator"></i></button>';
                        eliminar = '<button title="Eliminar renglón" data-renglon_id= "' + row.id + '" data-tipo="eliminar" onclick="app.eliminar_renglon(this)" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';

                        return '<center>'+editar + ' ' + operaciones + ' ' + eliminar+'</center>';
                    }
                },
            ]
        }
    }
    // settings_table_operaciones() {
    //     return {
    //         order: [
    //             [0, "asc"]
    //         ],
    //         aLengthMenu: [10],
    //         language: {
    //             url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json'
    //         },
    //         columns: [{
    //                 title: '#',
    //                 render: function(data, type, row) {
    //                     return parseInt(row.orden)
    //                 }
    //             },
    //             {
    //                 title: 'Renglón',
    //                 render: function(data, type, row) {
    //                     return row.clave_cuenta + '-' + row.nombre_cuenta
    //                 }
    //             },
    //             {
    //                 title: 'Tipo operación',
    //                 data: 'tipo_operacion'
    //             },
    //             {
    //                 title: 'Acciones',
    //                 width: '120px',
    //                 render: function(data, type, row) {
    //                     let editar = '';
    //                     let eliminar = '';
    //                     editar = '<button title="Editar cuenta" data-operacion_id= "' + row.id + '" data-renglon_id= "' + row.reporte_reglon_id + '" data-cuenta_id= "' + row.cuenta_id + '" data-tipo_operacion= "' + row.tipo_operacion + '" onclick="app.open_modal_operacion(this)" class="btn btn-ligth btn-sm"><i class="fas fa-pencil-alt"></i></button>';
    //                     eliminar = '<button title="Eliminar operación" data-operacion_id= "' + row.id + '" data-tipo="eliminar" data-renglon_id= "' + row.reporte_reglon_id + '" onclick="app.eliminar_operacion(this)" class="btn btn-ligth btn-sm"><i class="fas fa-trash"></i></button>';
    //                     return editar + ' ' + eliminar;
    //                 }
    //             },
    //         ]
    //     }
    // }
    get_busqueda() {
        $.ajax({
            dataType: "json",
            type: 'GET',
            dataSrc: "",
            url: PATH + '/administrador/api/reportes/listado_contenido?reporte_id=' + reporte_id,
            success: function(response) {
                let listado = $(table).DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    }
    get_busqueda_operaciones(renglon_id) {
        $.ajax({
            dataType: "json",
            type: 'GET',
            dataSrc: "",
            url: PATH + '/administrador/api/reportes/listado_operacions_by_renglon?renglon_id=' + renglon_id,
            success: function(response) {
                let listado = $(table_operaciones).DataTable();
                listado.clear().draw();
                if (response.data && response.data.length > 0) {
                    response.data.forEach(listado.row.add);
                    listado.draw();
                }
            }
        });
    }
    open_modal_cuenta(_this) {
        let renglon_id = $(_this).data('renglon_id');
        if (!renglon_id) {
            $("div#modal_cuenta .modal-title").html('Agregar cuenta');
            $("form#modal_cuentas input[name='nombre_cuenta']").val('');
            $("form#modal_cuentas input[name='renglon_id']").val('');
            $("form#modal_cuentas select[name='tipo_renglon']").val('');
            $("form#modal_cuentas select[name='tipo_formato']").val('');
        } else {
            $("div#modal_cuenta .modal-title").html('Editar cuenta');
            $("form#modal_cuentas input[name='nombre_cuenta']").val($(_this).data('nombre'));
            $("form#modal_cuentas input[name='renglon_id']").val($(_this).data('renglon_id'));
            $("form#modal_cuentas select[nam*='tipo_formato']").val($(_this).data('formato'));
            $("form#modal_cuentas select[name='tipo_renglon']").val($(_this).data('tipo_renglon'));
        }
        $("#modal_cuenta").modal("show");
    }
    open_modal_operacion(_this) {
        // $("select[name*='cuenta_id']").select2({
        //     dropdownParent: $('#modal_operaciones')
        // });
        
        let renglon_id = $(_this).data('renglon_id');
        $("select[name*='cuenta_id']").val('');
        $("select[name*='tipo_operacion']").val('');
        $("input[name*='operacion_id_']").val('');

        if (!renglon_id) {
            $(".modal-title").html('Agregar operación');
        } else {
            $(".modal-title").html('Editar operación');
            $("select[name*='cuenta_id']").val($(_this).data('tipo_operacion'));
            $("select[name*='tipo_operacion']").val($(_this).data('tipo_operacion'));
            $("input[name*='operacion_id_']").val($(_this).data('operacion_id'));
        }
        
    }

    open_operaciones(_this) {
        reporte_renglon_id = $(_this).data('renglon_id');
        this.open_operaciones_load(reporte_renglon_id);
    }

    open_operaciones_load(reporte_renglon_id) {
        $('div#modal_fomulas_reglon div.modal-body').html('<center><h4><i class="fas fa-spinner fa-pulse"></i> Cargando ...</h4></center>');

        $('div#modal_fomulas_reglon').modal({
            backdrop: 'static',
            keyboard: false
        });

        $.ajax({
            url: PATH + '/administrador/api/reportes/listado_operacions_by_renglon',
            dataType: "json",
            type: 'GET',
            data: {renglon_id: reporte_renglon_id},
            success: function(response) {
                //console.log(response);

                $('div#modal_fomulas_reglon div.modal-body').html( atob(response.html) );

            }
        });
    }
    regresar_principal(_this) {
        $("#principal_tabla").show();
        $("#operaciones_tabla").hide();
        app.get_busqueda();
    }

    agregar_fomula(_this){
        reporte_renglon_id = $(_this).data('renglon_id');

        $('div#modal_operaciones').modal({
            show: false,
            backdrop: 'static',
            keyboard: false
        });

        $('div#modal_operaciones').on('show.bs.modal', function () {
            $('div#modal_fomulas_reglon').modal('hide');
        });

        $('div#modal_operaciones').on('hidden.bs.modal', function () {
            app.open_operaciones_load(reporte_renglon_id);
        });

        $('div#modal_operaciones div.modal-header').html('Agregar operación');
        $('div#modal_operaciones div.modal-body').html('<center><h4><i class="fas fa-spinner fa-pulse"></i> Cargando ...</h4></center>');
        $('div#modal_operaciones').modal('show');

        
        $.ajax({
            url: PATH + '/administrador/api/reportes/modal_agregar_operacion',
            dataType: "json",
            type: 'GET',
            data: {renglon_id: reporte_renglon_id},
            success: function(response) {
                //console.log(response);

                $('div#modal_operaciones div.modal-body').html( atob(response.html) );

            }
        });
        
    }

    listado_reglones_reportes(_this){
        $('div#div_renglon_id').show();

        var act_reporte_id = $('select[name=reporte_id] option:selected').val();
  
        $.ajax({
            url: PATH + '/administrador/api/reportes/listado_reglones_reportes',
            dataType: "json",
            type: 'GET',
            data: {reporte_id: act_reporte_id},
            success: function(response) {
                try{
                    $.each( response.data, function( key, value ) {
                        $('select#renglon_id').append('<option value="'+value.id+'" >'+value.nombre+'</option>');
                    });
                }catch(e){}
            }
        });
        
    }

    

    cambiar_origen_informacion(_this){
        var opcion = _this.value;

        $('div#div_cuenta_id').hide();
        $('div#div_reporte_id').hide();
        $('div#div_renglon_id').hide();

        $('select#renglon_id option').remove();

        if(opcion == 1){
            $('div#div_cuenta_id').show();
        }else if(opcion == 3){
            $('div#div_reporte_id').show();
            $('div#div_renglon_id').show();

            $('select[name=reporte_id] option[value='+reporte_id+']').prop('selected',true);
            app.listado_reglones_reportes();

        }else{
            $('select[name=reporte_id] option[value='+reporte_id+']').prop('selected',true);
            app.listado_reglones_reportes();
        }

    }

    actualizar_tipo_renglon(){
        if($('select[name=tipo_renglon] option:selected').val() == 1){
            $('div#div_tipo_formato').show();
        }else{
            $('div#div_tipo_formato').hide();
        }
    }

    guardar_cuenta(_this) {
        $.ajax({
            url: PATH + '/administrador/api/reportes/save_cuenta',
            type: 'POST',
            data: {
                reporte_id: $("input#reporte_id").val(),
                renglon_id: $("input[name*='renglon_id']").val(),
                nombre_cuenta: $("input[name*='nombre_cuenta']").val(),
                tipo_formato: $("select[name*='tipo_formato']").val(),
                tipo_renglon: $("select[name*='tipo_renglon']").val()
            },
            success: function(response) {
                if (response.estatus != "error") {
                    $("#modal_cuenta").modal("hide");
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito!',
                        text: response.mensaje,
                    }).then((result) => {
                        app.get_busqueda();
                    });

                } else {
                    if (response.errores) {
                        $.each(response.errores, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: response.mensaje,
                        })
                    }
                }
            },
            error: function(respuesta) {
                console.log(respuesta);
            }
        });
    }

    eliminar_renglon(_this) {
        Swal.fire({
            title: 'Desea eliminar el renglón?',
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: 'Aceptar',
            denyButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: PATH + '/administrador/api/reportes/delete_renglon',
                    type: 'POST',
                    data: {
                        renglon_id: $(_this).data('renglon_id'),
                    },
                    success: function(response) {
                        if (response.estatus != "error") {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito!',
                                text: response.mensaje,
                            }).then((result) => {
                                app.get_busqueda();
                            });

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.mensaje,
                            })
                        }
                    },
                });
            } else {
                return false;
            }
        })
    }
    eliminar_operacion(_this) {
        Swal.fire({
            title: 'Desea eliminar la operación?',
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: 'Aceptar',
            denyButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: PATH + '/administrador/api/reportes/delete_operacion',
                    type: 'POST',
                    data: {
                        operacion_id: $(_this).data('operacion_id'),
                    },
                    success: function(response) {
                        if (response.estatus != "error") {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito!',
                                text: response.mensaje,
                            }).then((result) => {
                                app.get_busqueda_operaciones($(_this).data('renglon_id'));
                            });

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.mensaje,
                            })
                        }
                    },
                });
            } else {
                return false;
            }
        })
    }
    guardar_operacion(_this) {
        $.ajax({
            url: PATH + '/administrador/api/reportes/save_operacion',
            type: 'POST',
            data: $('form#form_operaciones').serializeArray(),
            success: function(response) {
                if (response.estatus != "error") {
                    $("#modal_operaciones").modal("hide");
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: 'Éxito!',
                    //     text: response.mensaje,
                    // }).then((result) => {
                    //     app.get_busqueda_operaciones(reporte_renglon_id);
                    // });

                } else {
                    if (response.errores) {
                        $.each(response.errores, function(index, value) {
                            if ($("small#msg_" + index).length) {
                                $("small#msg_" + index).html(value);
                            }
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: response.mensaje,
                        })
                    }
                }
            },
            error: function(respuesta) {
                console.log(respuesta);
            }
        });
    }
}

$(function() {
    app = new Funciones();
    app.create_table();
    //app.create_table_operaciones();
    //app.get_busqueda();


    //$('[title]').tooltip();

});