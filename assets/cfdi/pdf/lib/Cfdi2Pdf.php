<?php
/**
 * Librería para generación de versión impresa
 * de un CFDI (PDF) v3.2 y v.3.3
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.3.6 (27/04/2018)
 */

require_once 'vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

class Cfdi2Pdf {
    public $autor;
    public $titulo;
    public $asunto;
    public $tipoComprobante;
    public $direccionExpedicion;
    public $nombreArchivo = 'cfdi-pdf.pdf';
    public $piePagina;
    public $logo;
    public $mensajeFactura;
    public $encabezado;
    public $mensajeSello;
    
    public $pdfTemplate;
    public $templateDir;

    private $xml;


    public function __construct($templateDir=null){
        if($templateDir === null) {
            $templateDir = dirname(__FILE__).'/templates/';
        }
        $this->templateDir = $templateDir;
    }

    public function cargarArchivoXml($archivo) {
        if(!file_exists($archivo)) {
            return false;
        }
        $xmlContent = file_get_contents($archivo);
        $this->xml = Xml::parse($xmlContent);
        return $this->validarXml();
    }
    
    public function cargarCadenaXml($xmlContent) {
        if(empty($xmlContent)) {
            return false;
        }
        $this->xml = Xml::parse($xmlContent);
        
        return $this->validarXml();
    }

    public function generarPdf($descargar=false){
        $obj = $this->pdfObj();
        if(!$obj) {
            return false;
        }
        $obj->Output($this->nombreArchivo, $descargar ? 'D' : 'I');
        return true;
    }

    public function guardarPdf($path){
        $obj = $this->pdfObj();
        if(!$obj) {
            return false;
        }
        return $obj->Output($path.'/'.$this->nombreArchivo, 'F') === '';
    }

    public function obtenerPdf() {
        $obj = $this->pdfObj();
        if(!$obj) {
            return false;
        }
        return $obj->Output($this->nombreArchivo, 'S');
    }

    private function tipoCFDI() {
        $xml = $this->xml->getObject();
        $versionXml = $xml->getAttribute('Version', false) ?: $xml->getAttribute('version', false);
        if($versionXml === '3.2') {
            if($xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->name) {
                return 'cfdi32_nomina12';
            }elseif($xml->getChildren('cfdi:Complemento')->getChildren('nomina:Nomina')->name) {
                return 'cfdi32_nomina11';
            }else{
                return 'cfdi32';
            }
        }elseif($versionXml === '3.3') {
            if($xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->name) {
                return 'cfdi33_nomina12';
            }else{
                return 'cfdi33';
            }
        }else{
            return null;
        }
    }

    private function validarXml() {
        switch ($this->tipoCFDI()) {
            case 'cfdi32':
                $this->tipoComprobante = 'Factura';
                $this->pdfTemplate = 'cfdi32.php';
                return true;
            case 'cfdi33':
                $this->tipoComprobante = 'Factura';
                $this->pdfTemplate = 'cfdi33.php';
                return true;
            case 'cfdi32_nomina11':
                $this->tipoComprobante = 'Recibo de Nómina';
                $this->pdfTemplate = 'cfdi32_nomina11.php';
                return true;
            case 'cfdi32_nomina12':
                $this->tipoComprobante = 'Recibo de Nómina';
                $this->pdfTemplate = 'cfdi32_nomina12.php';
                return true;
            case 'cfdi33_nomina12':
                $this->tipoComprobante = 'Recibo de Nómina';
                $this->pdfTemplate = 'cfdi33_nomina12.php';
                return true;
            default:
                return false;
        }
    }

    private function pdfObj() {
        $file = $this->templateDir . $this->pdfTemplate;

        if(!empty($this->logo) && file_exists($this->logo)) {
            $logo = $this->logo;
        }else{
            $logo = null;
        }

        $content = self::renderPhpFile($file, array(
            'cfdi' => new PdfCfdi33($this->xml->getObject()),

            // requerido
            'xmlParser' => $this->xml,
            'tipoComprobante' => $this->tipoComprobante,
            'colorFondo' => '#DEF',
            'colorTexto' => '#000',
            'encabezado' => $this->encabezado,

            // opcional
            'mensajeFactura' => $this->mensajeFactura,
            'direccionExpedicion' => $this->direccionExpedicion,
            'piePagina' => $this->piePagina,
            'logo' => $logo,
            'mensajeSello' => $this->mensajeSello,
        ));

        if(!$content) {
            return false;
        }

        $html2pdf = new HTML2PDF('P', 'LETTER', 'es', true, 'UTF-8', 0);
        $html2pdf->pdf->SetDisplayMode('fullpage');

        $html2pdf->pdf->SetAuthor( $this->autor );
        $html2pdf->pdf->SetTitle( $this->titulo );
        $html2pdf->pdf->SetSubject( $this->asunto );

        $html2pdf->writeHTML($content);

        return $html2pdf;
    }

    private static function renderPhpFile($_file_, $_params_ = []) {
        if(!file_exists($_file_)) {
            return false;
        }
        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        require $_file_;
        return ob_get_clean();
    }
}

class PdfCfdi33 {
    public $xml;


    public function __construct($xml){
        $this->xml = $xml;
    }

    public function timbrado() {
        return $this->xml->getChildren('cfdi:Complemento')
            ->getChildren('tfd:TimbreFiscalDigital')
                ->getAttribute('UUID') != null;
    }
    public function formatDecimal($val, $minDigits=2) {
        $num = trim((string)$val, ' 0,');
        $parts = explode('.', $num);
        $ent = empty($parts[0]) ? '0' : $parts[0];
        $dec = empty($parts[1]) ? '0' : $parts[1];
        return number_format($ent, 0, '', ',')
            .'.'
            .str_pad($dec, max(strlen($dec), $minDigits), '0', STR_PAD_RIGHT)
            ;
    }

    public function monedaCFDI() {
        return $this->xml->getAttribute('Moneda', false);
    }

    public function totalConLetra() {
        $total = $this->xml->getAttribute('Total');
        $monedaClave = $this->monedaCFDI();

        $monedaSing = '';
        switch ($monedaClave) {
            case 'USD':
                $monedaSing = 'dolar';
                break;
            case 'MXN':
            default:
                $monedaSing = 'peso';
                break;
        }

        return CantidadConLetra::convertir($total, $monedaSing, $monedaClave);
    }

    public function getCadenaOriginalTFD(){
        $tfd = $this->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital');
        $uuid  = $tfd->getAttribute('UUID');
        $fecha = $tfd->getAttribute('FechaTimbrado');
        $sello = $tfd->getAttribute('SelloCFD', '');
        $cer   = $tfd->getAttribute('NoCertificadoSAT', '');
        $version = '1.0';
        return '||'.$version.'|'.$uuid.'|'.$fecha.'|'.$sello.'|'.$cer.'||';
    }
    public function getSerieFolio(){
        $serie = $this->xml->getAttribute('Serie', '');
        $folio = $this->xml->getAttribute('Folio', '');

        if(!empty($serie)){
            return $serie.' - '.$folio;
        }else{
            return $folio;
        }
    }
    public function getQr(){
        $total = $this->xml->getAttribute('Total');
        $re    = $this->xml->getChildren('cfdi:Emisor')->getAttribute('Rfc');
        $rr    = $this->xml->getChildren('cfdi:Receptor')->getAttribute('Rfc');
        $id    = $this->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID');
        $total = str_pad( sprintf("%.6f", (double)$total), 17, '0', STR_PAD_LEFT );
        $url = 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx';
        return $url.'?re='.$re.'&rr='.$rr.'&tt='.$total.'&id='.$id;
    }
    public function getImpuestos($row){
        $res = [];

        $impuestos = $row->getChildren('cfdi:Impuestos')->getChildren('cfdi:Traslados')->children;
        if($impuestos){
            foreach ($impuestos as $impuesto) {
                $val = $this->getTipoImpuesto($impuesto->getAttribute('Impuesto'), ' ')
                    .' '.$impuesto->getAttribute('TipoFactor');
                if($impuesto->getAttribute('TipoFactor') != 'Exento') {
                    $val .=
                        ' '.$impuesto->getAttribute('TasaOCuota')
                        .' $'.$impuesto->getAttribute('Importe');
                }

                $res[] = $val;
            }
        }

        $impuestos = $row->getChildren('cfdi:Impuestos')->getChildren('cfdi:Retenciones')->children;
        if($impuestos){
            foreach ($impuestos as $impuesto) {
                $res[] =
                    $this->getTipoImpuesto($impuesto->getAttribute('Impuesto'), ' ')
                    .' '.$impuesto->getAttribute('TipoFactor')
                    .' '.$impuesto->getAttribute('TasaOCuota')
                    .' $'.$impuesto->getAttribute('Importe');
            }
        }

        return implode(', ', $res);
    }
    public function getFormaPago($sep=' - ') {
        $clave = $this->xml->getAttribute('FormaPago');

        $cat = array(
            '01'=>'Efectivo',
            '02'=>'Cheque nominativo',
            '03'=>'Transferencia electrónica de fondos',
            '04'=>'Tarjeta de crédito',
            '05'=>'Monedero electrónico',
            '06'=>'Dinero electrónico',
            '08'=>'Vales de despensa',
            '12'=>'Dación en pago',
            '13'=>'Pago por subrogación',
            '14'=>'Pago por consignación',
            '15'=>'Condonación',
            '17'=>'Compensación',
            '23'=>'Novación',
            '24'=>'Confusión',
            '25'=>'Remisión de deuda',
            '26'=>'Prescripción o caducidad',
            '27'=>'A satisfacción del acreedor',
            '28'=>'Tarjeta de débito',
            '29'=>'Tarjeta de servicios',
            '30'=>'Aplicación de anticipos',
            '99'=>'Por definir',
        );

        if(array_key_exists($clave, $cat)) {
            return $clave.$sep.$cat[$clave];
        }
        return $clave;
    }
    public function getMetodoPago($sep=' - ') {
        $clave = $this->xml->getAttribute('MetodoPago');

        $cat = array(
            'PUE'=>'Pago en una sola exhibición',
            'PPD'=>'Pago en parcialidades o diferido',
        );

        if(array_key_exists($clave, $cat)) {
            return $clave.$sep.$cat[$clave];
        }
        return $clave;
    }
    public function getRegimenFiscal($sep=' - ') {
        $clave = $this->xml->getChildren('cfdi:Emisor')->getAttribute('RegimenFiscal');
        $cat = array(
            '601'=>'General de Ley Personas Morales',
            '603'=>'Personas Morales con Fines no Lucrativos',
            '605'=>'Sueldos y Salarios e Ingresos Asimilados a Salarios',
            '606'=>'Arrendamiento',
            '608'=>'Demás ingresos',
            '609'=>'Consolidación',
            '610'=>'Residentes en el Extranjero sin Establecimiento Permanente en México',
            '611'=>'Ingresos por Dividendos (socios y accionistas)',
            '612'=>'Personas Físicas con Actividades Empresariales y Profesionales',
            '614'=>'Ingresos por intereses',
            '616'=>'Sin obligaciones fiscales',
            '620'=>'Sociedades Cooperativas de Producción que optan por diferir sus ingresos',
            '621'=>'Incorporación Fiscal',
            '622'=>'Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras',
            '623'=>'Opcional para Grupos de Sociedades',
            '624'=>'Coordinados',
            '628'=>'Hidrocarburos',
            '607'=>'Régimen de Enajenación o Adquisición de Bienes',
            '629'=>'De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales',
            '630'=>'Enajenación de acciones en bolsa de valores',
            '615'=>'Régimen de los ingresos por obtención de premios',
        );

        if(array_key_exists($clave, $cat)) {
            return $clave.$sep.$cat[$clave];
        }
        return $clave;
    }
    public function getUsoCfdi($sep=' - ') {
        $clave = $this->xml->getChildren('cfdi:Receptor')->getAttribute('UsoCFDI');

        $cat = array(
            'G01'=>'Adquisición de mercancias',
            'G02'=>'Devoluciones, descuentos o bonificaciones',
            'G03'=>'Gastos en general',
            'I01'=>'Construcciones',
            'I02'=>'Mobilario y equipo de oficina por inversiones',
            'I03'=>'Equipo de transporte',
            'I04'=>'Equipo de computo y accesorios',
            'I05'=>'Dados, troqueles, moldes, matrices y herramental',
            'I06'=>'Comunicaciones telefónicas',
            'I07'=>'Comunicaciones satelitales',
            'I08'=>'Otra maquinaria y equipo',
            'D01'=>'Honorarios médicos, dentales y gastos hospitalarios.',
            'D02'=>'Gastos médicos por incapacidad o discapacidad',
            'D03'=>'Gastos funerales.',
            'D04'=>'Donativos.',
            'D05'=>'Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).',
            'D06'=>'Aportaciones voluntarias al SAR.',
            'D07'=>'Primas por seguros de gastos médicos.',
            'D08'=>'Gastos de transportación escolar obligatoria.',
            'D09'=>'Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.',
            'D10'=>'Pagos por servicios educativos (colegiaturas)',
            'P01'=>'Por definir',
        );

        if(array_key_exists($clave, $cat)) {
            return $clave.$sep.$cat[$clave];
        }
        return $clave;
    }
    public function getTipoComprobante($sep=' - ') {
        $clave = $this->xml->getAttribute('TipoDeComprobante');
        $cat = array(
            'I'=>'Ingreso',
            'E'=>'Egreso',
            'T'=>'Traslado',
            'N'=>'Nómina',
            'P'=>'Pago',
        );

        if(array_key_exists($clave, $cat)) {
            return $clave.$sep.$cat[$clave];
        }
        return $clave;
    }
    public function getTipoRelacion($sep=' - ') {
        $clave = $this->xml->getChildren('cfdi:CfdiRelacionados')->getAttribute('TipoRelacion');
        $cat = array(
            '01'=>'Nota de crédito de los documentos relacionados',
            '02'=>'Nota de débito de los documentos relacionados',
            '03'=>'Devolución de mercancía sobre facturas o traslados previos',
            '04'=>'Sustitución de los CFDI previos',
            '05'=>'Traslados de mercancias facturados previamente',
            '06'=>'Factura generada por los traslados previos',
            '07'=>'CFDI por aplicación de anticipo',
        );

        if(array_key_exists($clave, $cat)) {
            return $clave.$sep.$cat[$clave];
        }
        return $clave;
    }
    private function getTipoImpuesto($clave, $sep=' - ') {
        $cat = array(
            '001'=>'ISR',
            '002'=>'IVA',
            '003'=>'IEPS',
        );

        if(array_key_exists($clave, $cat)) {
            return $clave.$sep.$cat[$clave];
        }
        return $clave;
    }
}

class XmlElement {
  public $name;
  public $attributes;
  public $children;

  public function getChildren($name, $fallback='ELEMENT'){
    if(is_array($this->children)){
      foreach ($this->children as $child) {
        if($child->name == $name){
          return $child;
        }
      }
    }
    return $fallback !== 'ELEMENT' ? $fallback : new XmlElement;
  }
  public function getAttribute($name, $fallback=null){
    if(is_array($this->attributes)){
      if(isset($this->attributes[$name])){
        return $this->attributes[$name];
      }
    }
    return $fallback;
  }
}

class Xml {
  private $xml;

  private function __construct(){}

  public static function parse($xml){
    $tags = null;
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, $xml, $tags);
    xml_parser_free($parser);

    $elements = array();
    $stack = array();
    foreach ($tags as $tag) {
      if ($tag['type'] == 'complete' || $tag['type'] == 'open') {
        $index = count($elements);
        $elements[$index] = new XmlElement;
        $elements[$index]->name = $tag['tag'];
        $elements[$index]->attributes = isset($tag['attributes']) ? $tag['attributes'] : null;
        if ($tag['type'] == 'open') {  // push
          $elements[$index]->children = array();
          $stack[count($stack)] = &$elements;
          $elements = &$elements[$index]->children;
        }
      }
      if ($tag['type'] == 'close') {  // pop
        $elements = &$stack[count($stack) - 1];
        unset($stack[count($stack) - 1]);
      }
    }

    $clss = new self;
    $clss->xml = $elements[0];
    return $clss;
  }

  function getObject() {
    return $this->xml;
  }

  function getCadenaOriginalTFD(){
    $tfd = $this->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital');
    $uuid  = $tfd->getAttribute('UUID');
    $fecha = $tfd->getAttribute('FechaTimbrado');
    $sello = $tfd->getAttribute('selloCFD');
    $cer   = $tfd->getAttribute('noCertificadoSAT');
    $version = '1.0';
    return '||'.$version.'|'.$uuid.'|'.$fecha.'|'.$sello.'|'.$cer.'||';
  }
  function getSerieFolio(){
    return $this->xml->getAttribute('serie', '').
      $this->xml->getAttribute('folio', '');
  }
  function getQr(){
    $total = $this->xml->getAttribute('total');
    $re    = $this->xml->getChildren('cfdi:Emisor')->getAttribute('rfc');
    $rr    = $this->xml->getChildren('cfdi:Receptor')->getAttribute('rfc');
    $id    = $this->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID');

    $total = str_pad( sprintf("%.6f", (double)$total), 17, '0', STR_PAD_LEFT );
    return '?re='.$re.'&rr='.$rr.'&tt='.$total.'&id='.$id;
  }
  function getIvaRetenidoImporte(){
    $impuestos = $this->xml->getChildren('cfdi:Impuestos')->getChildren('cfdi:Retenciones')->children;
    $ivaSum = 0;
    if($impuestos){
      foreach ($impuestos as $impuesto) {
        if($impuesto->getAttribute('impuesto') == 'IVA'){
          $ivaSum += (float)$impuesto->getAttribute('importe');
        }
      }
    }
    return $ivaSum;
  }
  function getIsrRetenidoImporte(){
    $impuestos = $this->xml->getChildren('cfdi:Impuestos')->getChildren('cfdi:Retenciones')->children;
    $ivaSum = 0;
    if($impuestos){
      foreach ($impuestos as $impuesto) {
        if($impuesto->getAttribute('impuesto') == 'ISR'){
          $ivaSum += (float)$impuesto->getAttribute('importe');
        }
      }
    }
    return $ivaSum;
  }
  function getIvaImporte(){
    $impuestos = $this->xml->getChildren('cfdi:Impuestos')->getChildren('cfdi:Traslados')->children;
    $ivaSum = 0.0;
    if($impuestos){
      foreach ($impuestos as $impuesto) {
        if($impuesto->getAttribute('impuesto') == 'IVA'){
          $ivaSum += (float)$impuesto->getAttribute('importe');
        }
      }
    }
    return $ivaSum;
  }
  function getDomicilio($xmlArrayDomicilio){
    $parts = array();
    $parts2 = array();

    if(!empty($xmlArrayDomicilio['calle'])) $parts[] = $xmlArrayDomicilio['calle'];
    if(!empty($xmlArrayDomicilio['noExterior'])) $parts[] = '#'.$xmlArrayDomicilio['noExterior'];
    if(!empty($xmlArrayDomicilio['noInterior'])) $parts[] = ' (Int. '.$xmlArrayDomicilio['noInterior'].')';
    if(!empty($xmlArrayDomicilio['colonia'])) $parts[] = $xmlArrayDomicilio['colonia'];
    if(!empty($xmlArrayDomicilio['codigoPostal'])) $parts[] = 'C.P. '.$xmlArrayDomicilio['codigoPostal'];
    
    if(!empty($xmlArrayDomicilio['municipio'])) $parts2[] = $xmlArrayDomicilio['municipio'];
    if(!empty($xmlArrayDomicilio['estado'])) $parts2[] = $xmlArrayDomicilio['estado'];
    if(!empty($xmlArrayDomicilio['pais'])) $parts2[] = $xmlArrayDomicilio['pais'];

    // TODO: implementar localidad y referencia

    $str = '';
    if(!empty($parts)) $str .= implode(' ', $parts).', ';
    if(!empty($parts2)) $str .= implode(', ', $parts2);
    return $str;
  }
}

class CantidadConLetra {
    /**
     * Convierte un número en una cadena de letras
     * 
     * @param string $numero La cantidad numérica a convertir 
     * @param string $monedaSingular La moneda local de tu país (en singular)
     * @param string $sufijo Una cadena adicional para el sufijo
     * 
     * @return string La cantidad convertida a letras
     */
    public static function convertir($numero, $monedaSingular = 'PESO', $sufijo = 'MXN'){
        $monedaSingular = strtoupper($monedaSingular);
        $sufijo = strtoupper($sufijo);
        $monedaPlural = self::obtenerPlural($monedaSingular);

        $xarray = array(
            0 =>'CERO', 1=>'UN', 2=>'DOS', 3=>'TRES', 4=>'CUATRO', 5=>'CINCO', 6=>'SEIS', 7=>'SIETE', 8=>'OCHO', 9=>'NUEVE',
            10=>'DIEZ', 11=>'ONCE', 12=>'DOCE', 13=>'TRECE', 14=>'CATORCE', 15=>'QUINCE', 16=>'DIECISEIS', 17=>'DIECISIETE', 18=>'DIECIOCHO', 19=>'DIECINUEVE',
            20=>'VEINTI', 30=>'TREINTA', 40=>'CUARENTA', 50=>'CINCUENTA', 60=>'SESENTA', 70=>'SETENTA', 80=>'OCHENTA', 90=>'NOVENTA',
            100=>'CIENTO', 200=>'DOSCIENTOS', 300=>'TRESCIENTOS', 400=>'CUATROCIENTOS', 500=>'QUINIENTOS', 600=>'SEISCIENTOS', 700=>'SETECIENTOS', 800=>'OCHOCIENTOS', 900=>'NOVECIENTOS'
        );
 
        $numero = trim($numero);
        $xpos_punto = strpos($numero, '.');
        $xaux_int = $numero;
        $xdecimales = '00';
        if (!($xpos_punto === false)) {
            if ($xpos_punto == 0) {
                $numero = '0' . $numero;
                $xpos_punto = strpos($numero, '.');
            }
            $xaux_int = substr($numero, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
            $xdecimales = substr($numero . '00', $xpos_punto + 1, 2); // obtengo los valores decimales
        }
 
        $XAUX = str_pad($xaux_int, 18, ' ', STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
        $xcadena = '';
        for ($xz = 0; $xz < 3; $xz++) {
            $xaux = substr($XAUX, $xz * 6, 6);
            $xi = 0;
            $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
            $xexit = true; // bandera para controlar el ciclo del While
            while ($xexit) {
                if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
                    break; // termina el ciclo
                }
 
                $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
                $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
                for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
                    switch ($xy) {
                        case 1: // verifica las centenas
                            $key = (int) substr($xaux, 0, 3);
                            if ($key >= 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                                if (TRUE === array_key_exists($key, $xarray)) {  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                    $xseek = $xarray[$key];
                                    $xsub = self::obtenerSufijo($xaux); // devuelve el sufijo correspondiente (Millón, Millones, Mil o nada)
                                    if (100 == $key) {
                                        $xcadena = ' ' . $xcadena . ' CIEN ' . $xsub;
                                    } else {
                                        $xcadena = ' ' . $xcadena . ' ' . $xseek . ' ' . $xsub;
                                    }
                                    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                                } else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                    $key = (int) substr($xaux, 0, 1) * 100;
                                    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                    $xcadena = ' ' . $xcadena . ' ' . $xseek;
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 0, 3) < 100)
                            break;
                        case 2: // verifica las decenas (con la misma lógica que las centenas)
                            $key = (int) substr($xaux, 1, 2);
                            if ($key >= 10) {
                                if (TRUE === array_key_exists($key, $xarray)) {
                                    $xseek = $xarray[$key];
                                    $xsub = self::obtenerSufijo($xaux);
                                    if (20 == $key) {
                                        $xcadena = ' ' . $xcadena . ' VEINTE ' . $xsub;
                                    } else {
                                        $xcadena = ' ' . $xcadena . ' ' . $xseek . ' ' . $xsub;
                                    }
                                    $xy = 3;
                                } else {
                                    $key = (int) substr($xaux, 1, 1) * 10;
                                    $xseek = $xarray[$key];
                                    if (20 == $key)
                                        $xcadena = ' ' . $xcadena . ' ' . $xseek;
                                    else
                                        $xcadena = ' ' . $xcadena . ' ' . $xseek . ' Y ';
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 1, 2) < 10)
                            break;
                        case 3: // verifica las unidades
                            $key = (int) substr($xaux, 2, 1);
                            if ($key >= 1) { // si la unidad es cero, ya no hace nada
                                $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
                                $xsub = self::obtenerSufijo($xaux);
                                $xcadena = ' ' . $xcadena . ' ' . $xseek . ' ' . $xsub;
                            } // ENDIF (substr($xaux, 2, 1) < 1)
                            break;
                    } // END SWITCH
                } // END FOR
                $xi = $xi + 3;
            } // ENDDO
            # si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
            if ('ILLON' == substr(trim($xcadena), -5, 5)) {
                $xcadena .= ' DE';
            }
 
            # si la cadena obtenida en MILLONES o BILLONES, entonces le agrega al final la conjuncion DE
            if ('ILLONES' == substr(trim($xcadena), -7, 7)) {
                $xcadena .= ' DE';
            }
 
            # depurar leyendas finales
            if ('' != trim($xaux)) {
                switch ($xz) {
                    case 0:
                        if ('1' == trim(substr($XAUX, $xz * 6, 6))) {
                            $xcadena .= 'UN BILLON ';
                        } else {
                            $xcadena .= ' BILLONES ';
                        }
                        break;
                    case 1:
                        if ('1' == trim(substr($XAUX, $xz * 6, 6))) {
                            $xcadena .= 'UN MILLON ';
                        } else {
                            $xcadena .= ' MILLONES ';
                        }
                        break;
                    case 2:
                        if (1 > $numero) {
                            $xcadena = "CERO {$monedaPlural} {$xdecimales}/100 {$sufijo}";
                        }
                        if ($numero >= 1 && $numero < 2) {
                            $xcadena = "UN {$monedaSingular} {$xdecimales}/100 {$sufijo}";
                        }
                        if ($numero >= 2) {
                            $xcadena .= " {$monedaPlural} {$xdecimales}/100 {$sufijo}"; //
                        }
                        break;
                } // endswitch ($xz)
            } // ENDIF (trim($xaux) != "")
 
            $xcadena = str_replace('VEINTI ', 'VEINTI', $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
            $xcadena = str_replace('  ', ' ', $xcadena); // quito espacios dobles
            $xcadena = str_replace('UN UN', 'UN', $xcadena); // quito la duplicidad
            $xcadena = str_replace('  ', ' ', $xcadena); // quito espacios dobles
            $xcadena = str_replace('BILLON DE MILLONES', 'BILLON DE', $xcadena); // corrigo la leyenda
            $xcadena = str_replace('BILLONES DE MILLONES', 'BILLONES DE', $xcadena); // corrigo la leyenda
            $xcadena = str_replace('DE UN', 'UN', $xcadena); // corrigo la leyenda
        } // ENDFOR ($xz)
        return trim($xcadena);
    }
 
    /**
     * Regresa un sufijo para la cifra
     * 
     * @param string $cifras La cifra a medir su longitud
     * 
     * @return string El sufijo para la cifra
     */
    private static function obtenerSufijo($cifra){
        $cifra = trim($cifra);
        $strlen = strlen($cifra);
        return ($strlen >= 4 && $strlen <= 6)
            ? 'MIL'
            : '';
    }

    /**
     * Regresa el plural de una palabra
     * 
     * @param string $singular La palabra en singular
     * 
     * @return string La palabra en plural
     */
    private static function obtenerPlural($singular){
        $vocales = 'aeiouAEIOU';
        $ultima = substr($singular, -1);
        $sufijo = strpos($vocales, $ultima) === false ? 'es' : 's';

        if($ultima === strtoupper($ultima)){
          $sufijo = strtoupper($sufijo);
        }

        return $singular.$sufijo;
    }
}
